import 'dart:collection';

class TripDetails {
  int tripId;
  String startingLat;
  String startingLong;
  String dropLat;
  String dropLong;
  String tripStatus;
  String distance;
  String price;
  String driverStatus;
  String tripDate;

  TripDetails({
    this.tripId,
    this.startingLat,
    this.startingLong,
    this.dropLat,
    this.dropLong,
    this.tripStatus,
    this.distance,
    this.price,
    this.driverStatus,
    this.tripDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'TripId': tripId,
      'StartingLat': startingLat,
      'StartingLong': startingLong,
      'DropLat': dropLat,
      'DropLong': dropLong,
      'TripStatus': tripStatus ?? '',
      'Distance': distance,
      'Price': price,
      'DriverStatus': driverStatus ?? '',
      'TripDate': tripDate ?? DateTime.now().toString(),
    };
  }

  factory TripDetails.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TripDetails(
      tripId: map['TripId'],
      startingLat: map['StartingLat'],
      startingLong: map['StartingLong'],
      dropLat: map['DropLat'],
      dropLong: map['DropLong'],
      tripStatus: map['TripStatus'] ?? '',
      distance: map['Distance'],
      price: map['Price'],
      driverStatus: map['DriverStatus'] ?? '',
      tripDate: map['TripDate'] ?? DateTime.now().toString(),
    );
  }

  factory TripDetails.fromLinkedHashMap(LinkedHashMap<String, dynamic> map) {
    if (map == null) return null;

    return TripDetails(
      tripId: map['TripId'],
      startingLat: map['StartingLat'],
      startingLong: map['StartingLong'],
      dropLat: map['DropLat'],
      dropLong: map['DropLong'],
      tripStatus: map['TripStatus'] ?? '',
      distance: map['Distance'],
      price: map['Price'],
      driverStatus: map['DriverStatus'] ?? '',
      tripDate: map['TripDate'] ?? DateTime.now().toString(),
    );
  }
}
