import 'dart:collection';

class TripRequest {
  String tripCartId;
  String driverId;
  String requestId;
  TripRequest({
    this.tripCartId,
    this.driverId,
    this.requestId,
  });

  Map<String, dynamic> toMap() {
    return {
      'TripCartId': tripCartId,
      'DriverId': driverId,
      'RequestId': requestId,
    };
  }

  factory TripRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TripRequest(
      tripCartId: map['TripCartId'],
      driverId: map['DriverId'],
      requestId: map['RequestId'],
    );
  }

  factory TripRequest.fromLinkedHashMap(LinkedHashMap map) {
    if (map == null) return null;
    return TripRequest(
      tripCartId: map['TripCartId'],
      driverId: map['DriverId'],
      requestId: map['RequestId'],
    );
  }
}
