import 'dart:collection';

class Driver {
  int userId;
  String firstName;
  String lastName;
  String address;
  int companyId;
  String currentLat;
  String currentLong;
  String availability;
  int id;
  int status;
  String deviceToken;
  String photo;
  String systemId;
  Driver({
    this.userId,
    this.firstName,
    this.lastName,
    this.address,
    this.companyId,
    this.currentLat,
    this.currentLong,
    this.availability,
    this.id,
    this.status,
    this.deviceToken,
    this.photo,
    this.systemId,
  });

  Map<String, dynamic> toMap() {
    return {
      'UserId': userId,
      'FirstName': firstName,
      'LastName': lastName,
      'Address': address,
      'CompanyId': companyId,
      'CurrentLat': currentLat,
      'CurrentLong': currentLong,
      'Availability': availability ?? 'Not Available',
      'Id': id,
      'Status': status,
      'DeviceToken': deviceToken ?? '',
      'Photo': photo ?? '',
      'SystemId': systemId ?? ''
    };
  }

  factory Driver.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Driver(
      userId: map['UserId'],
      firstName: map['FirstName'],
      lastName: map['LastName'],
      address: map['Address'],
      companyId: map['CompanyId'],
      currentLat: map['CurrentLat'],
      currentLong: map['CurrentLong'],
      availability: map['Availability'] ?? 'Not Available',
      id: map['Id'],
      status: map['Status'],
      deviceToken: map['DeviceToken'] ?? '',
      photo: map['Photo'] ?? '',
      systemId: map['SystemId'] ?? '',
    );
  }

  factory Driver.fromLinkedHashMap(LinkedHashMap<dynamic, dynamic> map) {
    if (map == null) return null;

    return Driver(
      userId: map['UserId'],
      firstName: map['FirstName'],
      lastName: map['LastName'],
      address: map['Address'],
      companyId: map['CompanyId'],
      currentLat: map['CurrentLat'],
      currentLong: map['CurrentLong'],
      availability: map['Availability'] ?? 'Not Available',
      id: map['Id'],
      status: map['Status'],
      deviceToken: map['DeviceToken'] ?? '',
      photo: map['Photo'] ?? '',
      systemId: map['SystemId'] ?? '',
    );
  }
}
