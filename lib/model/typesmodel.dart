class VehicleTypesModel {
  final List<VehicleResult> result;

  VehicleTypesModel({
    this.result,
  });

  factory VehicleTypesModel.fromJson(Map<String, dynamic> json) {
    return VehicleTypesModel(
        // withError: json['error'],
        // shortMessage: json['message'],
        result: (json['Data'] as List)
            .map((e) => VehicleResult.fromJson(e))
            .toList());
  }
}

class VehicleResult {
  var id;
  var name;
  var img;
  var fare;
  var perkmFate;
  VehicleResult({this.fare, this.id, this.img, this.name, this.perkmFate});

  factory VehicleResult.fromJson(Map<String, dynamic> json) {
    return VehicleResult(
      id: json['VehicleId'] ?? "",
      img: json['Image'] ?? "",
      name: json['VehicleName'] ?? "",
      fare: json['StartingFare'] ?? "",
      perkmFate: json['PerKmFare'] ?? "",
    );
  }
}
