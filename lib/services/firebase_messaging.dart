import 'dart:io';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/model/trip_request.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class PushNotificationService {
  Future init({@required BuildContext context}) async {
    final FirebaseMessaging _fcm = FirebaseMessaging();
    TripRequest tripRequest = TripRequest();
    _fcm.setAutoInitEnabled(true);
    var tokenRefreshStream = _fcm.onTokenRefresh;
    tokenRefreshStream.listen((event) async {
      print(event);
    });
    String token = await _fcm.getToken();
    await setDeviceToken(token);

    String deviceToken = await getDeviceToken();

    print(deviceToken);
    if (token != null) {
      Provider.of<DriverProvider>(context, listen: false).changeToken(token);
    }
    var _dataProvider = Provider.of<DataProvider>(context, listen: false);
    var _driverProvider = Provider.of<DriverProvider>(context, listen: false);
    // setDeviceToken(token);

    //FIREBASE PUSH NOTIFICATION INITIALISATION AND CONFIGURATION
    if (_dataProvider.isPushNotificationEnabled) {
      if (token == deviceToken) {
        try {
          if (Platform.isIOS) {
            await _fcm.requestNotificationPermissions(
              const IosNotificationSettings(
                sound: true,
                badge: true,
                alert: true,
              ),
            );
            _fcm.onIosSettingsRegistered.listen((settings) async {
              print("settings:    $settings");
            });
          }
          _fcm.configure(
            onMessage: (Map<String, dynamic> message) async {
              print("on message: $message");
              // Provider.of<DriverProvider>(context, listen: false)
              //     .checkCurrentTripStatus(null);
              _dataProvider.setShowPopUp(true ?? false, message ?? {}, context);
              _driverProvider.setIsVehiclePicked(false);
              Provider.of<DriverProvider>(context, listen: false)
                  .checkCurrentTripStatus('');
              _driverProvider.changeDriverStatus('Available');
            },
            onResume: (Map<String, dynamic> message) async {
              print("on resume: $message");
              _dataProvider.setShowPopUp(true ?? false, message ?? {}, context);
              _driverProvider.setIsVehiclePicked(false);
              Provider.of<DriverProvider>(context, listen: false)
                  .checkCurrentTripStatus('');
              _driverProvider.changeDriverStatus('Available');
            },
            onLaunch: (Map<String, dynamic> message) async {
              print("on launch: $message");
              // var json=json.de
              Future.delayed(Duration(seconds: 2), () {
                _dataProvider.setShowPopUp(
                    true ?? false, message ?? {}, context);
                _driverProvider.setIsVehiclePicked(false);
                Provider.of<DriverProvider>(context, listen: false)
                    .checkCurrentTripStatus('');
                _driverProvider.changeDriverStatus('Available');
              });
            },
          );
        } on PlatformException {
          print('Error Found');
        }
      }
    }
  }
}
