import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:flutter_restart/flutter_restart.dart';
import '../language_constants.dart';

class TripDetailsWidget extends StatefulWidget {
  const TripDetailsWidget({Key key}) : super(key: key);

  @override
  _TripDetailsWidgetState createState() => _TripDetailsWidgetState();
}

class _TripDetailsWidgetState extends State<TripDetailsWidget> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _dataProvider = Provider.of<DataProvider>(context);
    var _driverProvider = Provider.of<DriverProvider>(context);
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _dataProvider.tripDetailsWidget,
            SizedBox(
              height: height * .01,
            ),
            _dataProvider.notificationWidget,
            SizedBox(
              height: height * .02,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 22,
              ),
              child: Row(
                children: [
                  Text(
                    // S.of(context).tripDetails,
                    getTranslated(context, 'tripDetails'),

                    style: nameTextStyle.copyWith(
                      fontSize: 22,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: height * .01,
            ),
            Container(
              width: width * .9,
              height: _driverProvider.currentTripStatus.toLowerCase() ==
                      'Completed'.toLowerCase()
                  ? height * .58
                  : height * .52,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 14.5,
                    spreadRadius: 1.5,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: height * .02,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: width * .04,
                      ),
                      Text(
                        // S.of(context).pickUpAddress,
                        getTranslated(context, 'pickUpAddress'),

                        style: tripStatusTextStyle.copyWith(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                      Spacer(),
                      SizedBox(
                        width: width * .05,
                        height: height * .05,
                        child: _driverProvider.currentTripStatus
                                    .toLowerCase() ==
                                'Picked'.toLowerCase()
                            ? Icon(
                                Icons.done,
                                color: Colors.black,
                              )
                            : _driverProvider.currentTripStatus.toLowerCase() ==
                                    'Completed'.toLowerCase()
                                ? Icon(
                                    Icons.done,
                                    color: Colors.black,
                                  )
                                : _driverProvider.currentTripStatus
                                            .toLowerCase() ==
                                        'Available'.toLowerCase()
                                    ? Icon(
                                        Icons.done,
                                        color: Colors.black,
                                      )
                                    : SvgPicture.asset(
                                        warning,
                                        fit: BoxFit.fitWidth,
                                      ),
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: height * .01,
                  ),
                  // _dataProvider.pickUpAddressWidget,

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              _driverProvider.currentTripStatus == ''
                                  ? Text(
                                      // S.of(context).waitingForAcceptance,
                                      getTranslated(
                                          context, 'waitingForAcceptance'),

                                      style: tripStatusTextStyle,
                                    )
                                  : Text(
                                      '${_driverProvider.pickUpAddress ?? ''}',
                                      style: tripStatusTextStyle,
                                    ),
                            ],
                          ),
                        ),
                        _driverProvider.currentTripStatus.toLowerCase() ==
                                'Accepted'.toLowerCase()
                            ? SizedBox(
                                width: width * .1,
                                height: width * .1,
                                child: GestureDetector(
                                  onTap: () {
                                    _dataProvider.pickUpFunction(context);
                                  },
                                  child: SvgPicture.asset(
                                    turn_right,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )
                            : SizedBox(),
                        SizedBox(
                          width: width * .01,
                        ),
                      ],
                    ),
                  ),

                  SizedBox(
                    height: _driverProvider.currentTripStatus.toLowerCase() ==
                            'Picked'.toLowerCase()
                        ? height * .01
                        : height * .03,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _driverProvider.currentTripStatus.toLowerCase() ==
                              "Accepted".toLowerCase()
                          ? InkWell(
                              borderRadius: BorderRadius.circular(10),
                              onTap: () async {
                                _driverProvider
                                    .initialiseProgressDialog(context);
                                _driverProvider.progressDialog.show();
                                var location = Location();
                                LocationData currentLocation =
                                    await location.getLocation();
                                await _driverProvider.updateCurrentLatLng(
                                    currentLocation.latitude.toString(),
                                    currentLocation.longitude.toString());
                                await _driverProvider.pickUpVehicle(context);
                                _driverProvider.progressDialog.hide();
                              },
                              child: Container(
                                height: height * .05,
                                width: width * .28,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: redColor,
                                ),
                                child: Center(
                                  child: Text(
                                    // S.of(context).pickUp,
                                    getTranslated(context, 'pickUp'),

                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                  SizedBox(
                    height: height * .05,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: width * .04,
                      ),
                      Text(
                        // S.of(context).dropOffAddress,
                        getTranslated(context, 'dropOffAddress'),

                        style: tripStatusTextStyle.copyWith(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                      Spacer(),
                      SizedBox(
                        width: width * .05,
                        height: height * .05,
                        child: _driverProvider.currentTripStatus
                                    .toLowerCase() ==
                                'Completed'.toLowerCase()
                            ? Icon(
                                Icons.done,
                                color: Colors.black,
                              )
                            : _driverProvider.currentTripStatus.toLowerCase() ==
                                    'Available'.toLowerCase()
                                ? Icon(
                                    Icons.done,
                                    color: Colors.black,
                                  )
                                : SvgPicture.asset(
                                    warning,
                                    fit: BoxFit.fitWidth,
                                  ),
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                    ],
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left: 16, right: 8),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              _driverProvider.currentTripStatus == ''
                                  ? Text(
                                      // S.of(context).waitingForAcceptance,
                                      getTranslated(
                                          context, 'waitingForAcceptance'),

                                      style: tripStatusTextStyle,
                                    )
                                  : _driverProvider.currentTripStatus
                                              .toLowerCase() ==
                                          'Accepted'.toLowerCase()
                                      ? Text(
                                          // S.of(context).waitingForAcceptance,
                                          getTranslated(
                                              context, 'waitingForAcceptance'),

                                          style: tripStatusTextStyle,
                                        )
                                      : Text(
                                          '${_driverProvider.dropOffAddress ?? ''}',
                                          style: tripStatusTextStyle,
                                        ),
                            ],
                          ),
                        ),
                        _driverProvider.currentTripStatus.toLowerCase() ==
                                'Picked'.toLowerCase()
                            ? SizedBox(
                                width: width * .1,
                                height: width * .1,
                                child: GestureDetector(
                                  onTap: () {
                                    _dataProvider.dropOffFunction(context);
                                  },
                                  child: SvgPicture.asset(
                                    turn_right,
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              )
                            : SizedBox(),
                        SizedBox(
                          width: width * .03,
                        ),
                      ],
                    ),
                  ),

                  SizedBox(
                    height: height * .03,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _driverProvider.currentTripStatus.toLowerCase() ==
                              "Picked".toLowerCase()
                          ? InkWell(
                              borderRadius: BorderRadius.circular(10),
                              onTap: () async {
                                _driverProvider
                                    .initialiseProgressDialog(context);
                                _driverProvider.progressDialog.show();
                                var location = Location();
                                LocationData currentLocation =
                                    await location.getLocation();
                                await _driverProvider.updateCurrentLatLng(
                                    currentLocation.latitude.toString(),
                                    currentLocation.longitude.toString());
                                await _driverProvider.tripComplete(context);
                                _driverProvider.progressDialog.hide();
                              },
                              child: Container(
                                height: height * .04,
                                width: width * .28,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: redColor,
                                ),
                                child: Center(
                                  child: Text(
                                    // S.of(context).dropped,
                                    getTranslated(context, 'dropped'),

                                    maxLines: 1,
                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                  _driverProvider.currentTripStatus.toLowerCase() ==
                          'Completed'.toLowerCase()
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            children: [
                              Text(
                                // S.of(context).totalAmount,
                                getTranslated(context, 'totalAmount'),

                                style: tripStatusTextStyle.copyWith(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Spacer(),
                              Text(
                                _driverProvider.tripDetails.price ?? '',
                                style: numericTextStyle.copyWith(
                                  fontSize: 30,
                                ),
                              ),
                              SizedBox(
                                width: width * .09,
                                height: width * .07,
                                child: Image.asset(
                                  money,
                                  color: mainColor3,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                  SizedBox(
                    height: height * .03,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _driverProvider.currentTripStatus.toLowerCase() ==
                              "Completed".toLowerCase()
                          ? InkWell(
                              borderRadius: BorderRadius.circular(10),
                              onTap: () async {
                                _driverProvider
                                    .initialiseProgressDialog(context);
                                await _driverProvider
                                    .cashReceived()
                                    .then((value) {
                                  setState(() {
                                    // Provider.of<MapBoxLocationProvider>(context,
                                    //         listen: false)
                                    //     .changeGoLive(true, context);
                                    // Provider.of<DriverProvider>(context,
                                    //         listen: false)
                                    //     .updateCurrentStatus("Not Available");
                                    // _driverProvider
                                    //     .checkCurrentTripStatus('No Trip');
                                    // Provider.of<DriverProvider>(context,
                                    //         listen: false)
                                    //     .updateCurrentStatus("Available");
                                    // _driverProvider
                                    //     .changeDriverStatus('Not Available');
                                  });
                                });
                              },
                              child: Container(
                                height: height * .04,
                                width: width * .42,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: redColor,
                                ),
                                child: Center(
                                  child: Text(
                                    // S.of(context).paymentReceived,
                                    getTranslated(context, 'paymentReceived'),

                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
