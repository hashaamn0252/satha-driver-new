import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/screens/main_screen.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:slider_button/slider_button.dart';
import 'package:flutter_restart/flutter_restart.dart';
import '../language_constants.dart';

class TripArrivedWidget extends StatefulWidget {
  TripArrivedWidget({Key key}) : super(key: key);

  @override
  _TripArrivedWidgetState createState() => _TripArrivedWidgetState();
}

class _TripArrivedWidgetState extends State<TripArrivedWidget> {
  // var _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _dataProvider = Provider.of<DataProvider>(context);
    var _driverProvider = Provider.of<DriverProvider>(context);
    var _locationProvider = Provider.of<MapBoxLocationProvider>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6),
          child: Text(
            // S.of(context).newTrip,
            getTranslated(context, 'newTrip'),

            style: nameTextStyle.copyWith(
              fontSize: 22,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(
          height: height * .01,
        ),
        Container(
          width: width * .9,
          height: height * .3,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
          ),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(
                    height: height * .03,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: greyColorDark,
                      boxShadow: [],
                    ),
                    height: height * .24,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: height * .02,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * .03,
                            ),
                            CircularCountDownTimer(
                              controller: _dataProvider.countDownController,
                              onComplete: () async {
                                // _driverProvider
                                //     .initialiseProgressDialog(context);
                                // _driverProvider.progressDialog.show();
                                // await _driverProvider.driverResponse(
                                //   response: 'ACCEPTED',
                                //   context: context,
                                // );
                                // _driverProvider.progressDialog.hide();

                                // _dataProvider.countDownController.restart();
                                await _driverProvider
                                    .checkCurrentTripStatus('No Trip');
                                // await _driverProvider.changeDriverStatus('Not Available');
                                // _locationProvider.changeGoLive(false, context);
                                FlutterRestart.restartApp();
                              },
                              width: width * .06,
                              height: width * .06,
                              duration: 30,
                              strokeWidth: 14,
                              isTimerTextShown: false,
                              fillColor: greyColorDark,
                              color: Colors.white,
                              isReverse: true,
                            ),
                            SizedBox(
                              width: width * .26,
                            ),
                            Text(
                              // S.of(context).newTrip,
                              getTranslated(context, 'newTrip'),

                              style: tripStatusTextStyle.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          // S.of(context).CustomerWaiting,
                          getTranslated(context, 'CustomerWaiting'),

                          style: tripStatusTextStyle.copyWith(
                            color: mainColor,
                          ),
                        ),
                        SizedBox(
                          height: height * .03,
                        ),
                        SliderButton(
                          action: () async {
                            _driverProvider.initialiseProgressDialog(context);
                            _driverProvider.progressDialog.show();
                            await _driverProvider.driverResponse(
                              response: 'ACCEPTED',
                              context: context,
                            );
                            _driverProvider.progressDialog.hide();
                          },
                          height: width * .18,
                          buttonSize: width * .18,
                          icon: Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                          boxShadow: BoxShadow(
                            color: Colors.transparent,
                          ),
                          dismissible: false,
                          label: Text(
                            'Swipe To Accept',
                            style: tripStatusTextStyle,
                          ),
                          backgroundColor: Colors.white,
                          buttonColor: Colors.black,
                        ),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.center,
                        //   children: [
                        //     InkWell(
                        //       onTap: () async {
                        //         _driverProvider.initialiseProgressDialog(context);
                        //         _driverProvider.progressDialog.show();
                        //         await _driverProvider.driverResponse(
                        //           response: 'ACCEPTED',
                        //           context: context,
                        //         );
                        //         _driverProvider.progressDialog.hide();
                        //       },
                        //       child: CircleAvatar(
                        //         backgroundColor: Colors.white,
                        //         maxRadius: 28,
                        //         child: Icon(
                        //           Icons.done,
                        //           color: Colors.black,
                        //           size: 32,
                        //         ),
                        //       ),
                        //     ),
                        //     SizedBox(
                        //       width: width * .1,
                        //     ),
                        //     InkWell(
                        //       onTap: () async {
                        //         _driverProvider.initialiseProgressDialog(context);
                        //         _driverProvider.progressDialog.show();
                        //         await _driverProvider.driverResponse(
                        //           response: 'REJECTED',
                        //           context: context,
                        //         );

                        //         await _driverProvider.checkCurrentTripStatus('No Trip');
                        //         await _driverProvider.changeDriverStatus('Not Available');
                        //         _locationProvider.changeGoLive(false, context);
                        //         _driverProvider.progressDialog.hide();
                        //         Navigator.of(context).pushAndRemoveUntil(
                        //             MaterialPageRoute(builder: (_) => MainScreen()),
                        //             (route) => false);
                        //       },
                        //       child: CircleAvatar(
                        //         maxRadius: 28,
                        //         backgroundColor: redColor,
                        //         child: Icon(
                        //           Icons.clear,
                        //           color: Colors.white,
                        //           size: 32,
                        // ),
                        // ),
                        // ),
                        // ],
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
              Positioned(
                top: height * .008,
                right: width * .0,
                child: GestureDetector(
                  onTap: () async {
                    _driverProvider.initialiseProgressDialog(context);
                    _driverProvider.progressDialog.show();
                    await _driverProvider.driverResponse(
                      response: 'REJECTED',
                      context: context,
                    );

                    await _driverProvider.checkCurrentTripStatus('No Trip');
                    // await _driverProvider.changeDriverStatus('Not Available');
                    // _locationProvider.changeGoLive(false, context);
                    _driverProvider.progressDialog.hide();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (_) => MainScreen()),
                        (route) => false);
                  },
                  child: Container(
                    width: width * .1,
                    height: width * .1,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(
                        width * .8,
                      ),
                    ),
                    child: Icon(
                      Icons.clear,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
