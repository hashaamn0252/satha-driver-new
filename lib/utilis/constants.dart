import 'package:SathaCaptain/main.dart';
import 'package:flutter/material.dart';
import '../language_constants.dart';

final googleApiKey = "AIzaSyB740Doi7PiV5TH8awJ4vmGaSn3gnlHipM";

final String BASE_URL = "http://sathaqtr.com/api/Downer/";
//  "http://3.130.232.238/breakDowner/api/Downer/";

final loginURL = BASE_URL + "driverLogin";
final Map<String, String> webAPIKey = {
  "x-api-key": "987654",
};
final fontFamily = "Open Sans Regular";
final forgotPasswordURL = BASE_URL + "forgotPassword";
final updatePasswordURL = BASE_URL + 'updateDriverPassword';
final updateCurrentLatLngURL = BASE_URL + "driverCurrentLocation";
final updateStatusURL = BASE_URL + "driverAvailabilityStatus";
final updateDeviceTokenURL = BASE_URL + "driverUpdateToken";
final driverDetailsURL = BASE_URL + "driverDetails";
final driverResponseURL = BASE_URL + "driverResponse";
final vehicleTypes = BASE_URL + "getVehicleTypes";
final driverRegistration = BASE_URL + "newDriverRegistration";
final driverRegistrationNext = BASE_URL + "driverRegistrationNext";
final sendMobileNo = BASE_URL + "sendDriverMobileNumber";
final mapBoxToken =
    "pk.eyJ1IjoiYnJpZ2h0bWVkaWFxdHIiLCJhIjoiY2toMzludDY1MG1uejJ1bHMwZWJsejg2MiJ9.hSDLbE40l1wWGdwocPz5SQ";
final pickUpURL = BASE_URL + "pickupVehicle";
final tripCompleteURL = BASE_URL + "completeTrip";
final checkDriverURL = BASE_URL + "checkDriverTrip";
final tripHistory = BASE_URL + "driverTripHistory";

final updateSystemIdURL = BASE_URL + 'driverUpdateSystemId';

final cashReceivedURL = BASE_URL + "receivedCash";
final driverStatusWhenKillURL = BASE_URL + "driverStatusWhenKill";

const int _pinkPrimaryValue = 0xff58595b;
const MaterialColor pink = MaterialColor(
  _pinkPrimaryValue,
  <int, Color>{
    50: Colors.white,
    100: Colors.white,
    200: Colors.white,
    300: Colors.white,
    400: Colors.white,
    500: Colors.white,
    600: Colors.white,
    700: Colors.white,
    800: Colors.white,
    900: Colors.white,
  },
);

void changeLanguage(String language, BuildContext context) async {
  Locale _locale = await setLocale(language);
  MyApp.setLocale(context, _locale);
}
