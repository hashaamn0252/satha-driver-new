import 'package:SathaCaptain/utilis/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

var nameTextStyle = GoogleFonts.montserrat(
  fontSize: 28,
  color: mainColor,
  // fontFamily: fontFamily,
);

var alertDialogTextStyle = GoogleFonts.montserrat(
  fontSize: 20,
  color: orangeColor,
  // fontFamily: fontFamily,
  fontWeight: FontWeight.bold,
);
TextStyle tripStatusTextStyle = GoogleFonts.montserrat(
  fontSize: 16,
  color: greyColorDark,
);

TextStyle numericTextStyle = GoogleFonts.orbitron(
  fontSize: 26,
  color: mainColor3,
);
