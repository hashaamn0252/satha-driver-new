import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

const mainColor = Color(0xffffb71c);
const redColor = Color(0xffc11f1c);
const mainColor2 = Color(0xffffa71a);
const greyColor = Color(0xffE2E3E7);
const greyColorDark = Color(0xff58595b);
const lightBlue = Color(0xff5b749c);
const orangeColor = Color(0xfff15a29);
const mainColor3 = Color(0xfff7941d);

Map<String, String> headers = {"Content-Type": "application/json"};
