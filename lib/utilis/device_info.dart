import 'dart:io';

import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';

class SystemInfo {
  Future deviceInfo() async {
    String message = '';
    String systemId;
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        systemId = androidDeviceInfo.androidId +
            androidDeviceInfo.fingerprint +
            androidDeviceInfo.board;
        setDeviceInfo(systemId);
        print(systemId);
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        systemId = iosDeviceInfo.identifierForVendor +
            iosDeviceInfo.utsname.machine +
            iosDeviceInfo.utsname.nodename;
        setDeviceInfo(systemId);
        print(systemId);
      }
    } on PlatformException {
      message = 'Error found';
      print(message);
    }
  }
}
