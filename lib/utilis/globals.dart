import 'dart:convert';

import 'package:SathaCaptain/bloc/connectivity.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'constants.dart';

BlocClass myBloc;

Future checkTripStatus(BuildContext context) async {
  var _dataProvider = Provider.of<DataProvider>(context, listen: false);
  String driverId = await getUserIdPrefs();
  var _driverProvider = Provider.of<DriverProvider>(context, listen: false);
  _driverProvider.checkCurrentTripStatus('No Trip');
  _driverProvider.changeDriverStatus('Available');
  await http.post(checkDriverURL, headers: {
    "x-api-key": "987654",
  }, body: {
    "DriverId": driverId,
  }).then((value) async {
    var response = json.decode(value.body);
    _dataProvider.changeDriverStatus(status: response['DriverStatus']);
    if (response['DriverStatus']
        .toUpperCase()
        .contains('On Ride'.toUpperCase())) {
      _driverProvider.changeTripDetails(response['Response']);
      _driverProvider
          .checkCurrentTripStatus(_driverProvider.tripDetails.tripStatus);
      _driverProvider
          .changeDriverStatus(_driverProvider.tripDetails.driverStatus);

      print(_driverProvider.currentTripStatus);

      // print(_driverProvider.tripDetails);
      final coordinatesForPickup = Coordinates(
        double.parse(
          _driverProvider.tripDetails.startingLat,
        ),
        double.parse(
          _driverProvider.tripDetails.startingLong,
        ),
      );
      var addresses = await Geocoder.local
          .findAddressesFromCoordinates(coordinatesForPickup);
      _driverProvider.changePickUpAddress(
        addresses.first.addressLine,
      );
      final coordinatesforDropOff = Coordinates(
        double.parse(
          _driverProvider.tripDetails.dropLat,
        ),
        double.parse(
          _driverProvider.tripDetails.dropLong,
        ),
      );
      var address = await Geocoder.local
          .findAddressesFromCoordinates(coordinatesforDropOff);
      _driverProvider.changeDropOffAddress(address.first.addressLine);
      print(_dataProvider.tripStatus);
    }
    print(_driverProvider.currentTripStatus);
  });
}

Future driverStatusChangeWhenAppKill() async {
  String driverId = await getUserIdPrefs() ?? '';
  if (driverId.isNotEmpty) {
    print("hello");
    // Provider.of<DriverProvider>(context, listen: false)
    //     .updateCurrentStatus("Not Available");
    http.post(
      driverStatusWhenKillURL,
      headers: webAPIKey,
      body: {
        "DriverId": driverId,
      },
    ).then((value) {
      var response = json.decode(value.body);
      print("Response ======>>>> $response");
    });
  }
}

PushNotificationService pushNotificationService;
void initalisePushNotifications({@required BuildContext context}) {
  PushNotificationService pushNotificationService = PushNotificationService();
  pushNotificationService.init(context: context);
}
