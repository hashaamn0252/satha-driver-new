final appLogo = "assets/images/appLogo.png";
final arabic = "assets/images/arabic.png";
final english = "assets/images/english.png";
final login_screen_picture = "assets/images/login_screen_picture.png";
final logo2 = "assets/images/logo2.png";
final menu_icon2 = "assets/images/menu_icon2.png";
final language_icon = "assets/images/language_icon.png";
final profile_avatar = "assets/images/profile_avatar.png";
final splash_screen_background = "assets/images/splash_screen_background.jpg";
final floating_dialog_background = "assets/images/float_dialog_background.png";
final mobile = "assets/images/mobile.jpg";
final drawer_background = "assets/images/drawer_background.jpg";
final profile_avataar = "assets/images/profile_avatar.jpg";
final sathaLogo = "assets/images/sathaLogo.png";
final delivery_truck = "assets/images/delivery_truck.png";
final pick_up_icon = "assets/images/pick_up_icon.png";
final warning = "assets/images/warning.svg";
final drop_off_icon = "assets/images/drop_off_icon.png";
final navigate = "assets/images/navigate.png";
final current_location = "assets/images/current_location.png";
final turn_right = "assets/images/turn_right.svg";
final money = "assets/images/money.png";
final source_destination_icon = "assets/images/source_destination_icon.png";
final waitingForTrip = "assets/images/waitingForTrip.gif";
final warning2 = "assets/images/warning.png";
final hourGlassGif = "assets/images/hourglass2.gif";
final satha_text = "assets/images/satha_text.png";
final splash_screen = "assets/images/splash_screen.jpg";
