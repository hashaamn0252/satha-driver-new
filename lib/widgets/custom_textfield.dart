import 'package:SathaCaptain/utilis/styles.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController textFieldController;
  final String title;
  final bool obsecure;
  final maxLength;
  final TextInputType textInputType;
  const CustomTextField(
      {Key key,
      @required this.textFieldController,
      @required this.title,
      @required this.obsecure,
      this.maxLength,
      this.textInputType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: TextField(
        controller: textFieldController,
        decoration: InputDecoration(
          hintText: title,
          hintStyle:
              tripStatusTextStyle.copyWith(color: Colors.grey, fontSize: 18),
        ),
        maxLength: maxLength,
        obscureText: obsecure,
        keyboardType: textInputType,
        style: tripStatusTextStyle.copyWith(
          color: Colors.black,
          fontSize: 18,
        ),
      ),
    );
  }
}
