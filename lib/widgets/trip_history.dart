import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/language_constants.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:flutter/material.dart';

class TripHistory extends StatelessWidget {
  TripHistory({
    Key key,
    @required this.pickupAddress,
    @required this.dropOffAddress,
    @required this.tripId,
    @required this.startDate,
    @required this.totalAmount,
    @required this.totalDistance,
  }) : super(key: key);
  final String pickupAddress,
      dropOffAddress,
      tripId,
      startDate,
      totalAmount,
      totalDistance;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: height * .2,
      width: width,
      child: Card(
        elevation: 6,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: height * .12,
                child: Image.asset(
                  source_destination_icon,
                  fit: BoxFit.fill,
                ),
              ),
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * .01,
                            ),
                            Expanded(
                              child: Text(
                                pickupAddress,
                                style: tripStatusTextStyle.copyWith(
                                  fontSize: 13,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: height * .00,
                      ),
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * .01,
                            ),
                            Expanded(
                              child: Text(
                                dropOffAddress,
                                style: tripStatusTextStyle.copyWith(
                                  fontSize: 13,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: width * .01,
                          ),
                          Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    // S.of(context).totalAmount,
                                    getTranslated(context, 'totalAmount'),
                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    width: width * .02,
                                  ),
                                  Text(
                                    totalAmount,
                                    maxLines: 1,
                                    overflow: TextOverflow.clip,
                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    // S.of(context).totalDistance,
                                    getTranslated(context, 'totalDistance'),
                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    width: width * .02,
                                  ),
                                  Text(
                                    totalDistance,
                                    maxLines: 1,
                                    overflow: TextOverflow.clip,
                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: height * .005,
                      ),
                    ],
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      'T# $tripId',
                      style: tripStatusTextStyle.copyWith(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * .003,
                  ),
                  Text(
                    startDate,
                    style: tripStatusTextStyle.copyWith(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: width * .03,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
