import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/constants.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:SathaCaptain/widgets/restart_app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../language_constants.dart';
import '../main.dart';

bottomSheet(BuildContext context) {
  double width = MediaQuery.of(context).size.width;
  double height = MediaQuery.of(context).size.height;
  var _dataProvider = Provider.of<DataProvider>(context, listen: false);

  showModalBottomSheet(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(20),
      ),
    ),
    clipBehavior: Clip.antiAliasWithSaveLayer,
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (BuildContext context, setState) {
          return Container(
            height: height * .23,
            padding: EdgeInsets.only(
              top: 10,
              bottom: 10,
              left: 10,
              right: 10,
            ),
            child: Column(
              children: [
                SizedBox(
                  height: height * .01,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: Row(
                    children: [
                      SizedBox(
                        width: width * .06,
                        height: width * .06,
                        child: Image.asset(
                          language_icon,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: width * .02,
                      ),
                      Text(
                        // S.of(context).changeLanguage,
                        getTranslated(context, 'changeLanguage'),

                        style: tripStatusTextStyle.copyWith(
                          fontSize: 22,
                          color: Colors.blue[900],
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: height * .01,
                ),
                InkWell(
                  onTap: () {
                    changeLanguage('ar', context);
                    _dataProvider.changeLanguage('العربية');
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: height * .05,
                        ),
                        Icon(
                          Icons.language,
                          color:
                              Provider.of<DataProvider>(context, listen: false)
                                          .language ==
                                      "العربية"
                                  ? Colors.black
                                  : Colors.grey,
                          size: 20,
                        ),
                        SizedBox(
                          width: width * .01,
                        ),
                        Text(
                          // S.of(context).arabic,
                          getTranslated(context, 'arabic'),

                          style: tripStatusTextStyle.copyWith(
                              fontSize: 16,
                              color: Provider.of<DataProvider>(context,
                                              listen: false)
                                          .language ==
                                      "العربية"
                                  ? Colors.black
                                  : Colors.grey),
                        ),
                        Spacer(),
                        Provider.of<DataProvider>(context, listen: false)
                                    .language ==
                                "العربية"
                            ? Icon(
                                Icons.done,
                                size: 20,
                                color: Provider.of<DataProvider>(context,
                                                listen: false)
                                            .language ==
                                        "العربية"
                                    ? Colors.black
                                    : Colors.grey,
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    changeLanguage('en', context);
                    _dataProvider.changeLanguage('English');
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: height * .05,
                        ),
                        Icon(
                          Icons.language,
                          color:
                              Provider.of<DataProvider>(context, listen: false)
                                          .language ==
                                      "English"
                                  ? Colors.black
                                  : Colors.grey,
                          size: 20,
                        ),
                        SizedBox(
                          width: width * .01,
                        ),
                        Text(
                          // S.of(context).english,
                          getTranslated(context, 'english'),

                          style: tripStatusTextStyle.copyWith(
                              fontSize: 16,
                              color: Provider.of<DataProvider>(context,
                                              listen: false)
                                          .language ==
                                      "English"
                                  ? Colors.black
                                  : Colors.grey),
                        ),
                        Spacer(),
                        Provider.of<DataProvider>(context, listen: false)
                                    .language ==
                                "English"
                            ? Icon(
                                Icons.done,
                                size: 20,
                                color: Provider.of<DataProvider>(context,
                                                listen: false)
                                            .language ==
                                        "English"
                                    ? Colors.black
                                    : Colors.grey,
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
    },
  );
}
