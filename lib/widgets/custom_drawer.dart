import 'package:SathaCaptain/app_router.dart';
import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/constants.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../language_constants.dart';
import 'custom_bottom_sheet.dart';

class CustomDrawer extends StatelessWidget {
  final _navigationService = locator<NavigationService>();
  final VoidCallback endDrawer;

  CustomDrawer({Key key, this.endDrawer}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var _dataProvider = Provider.of<DataProvider>(context);
    var _driverProvider = Provider.of<DriverProvider>(context);
    var _locationProvider = Provider.of<MapBoxLocationProvider>(context);
    return SafeArea(
      child: Container(
        alignment: Alignment.topRight,
        width: width * .65,
        height: height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            width * .04,
          ),
          color: Colors.white,
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: width * .8,
                child: Stack(
                  children: [
                    Container(
                      width: width * .65,
                      height: height * .3,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            // topLeft: Radius.circular(
                            //   80,
                            // ),
                            ),
                        image: DecorationImage(
                          image: AssetImage(
                            drawer_background,
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      top: height * .01,
                      width: width * .65,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          SizedBox(
                            width: width * .15,
                            child: ClipOval(
                              child: Material(
                                child: InkWell(
                                  child: _dataProvider.language == "العربية"
                                      ? Image.asset(
                                          arabic,
                                          fit: BoxFit.cover,
                                        )
                                      : Image.asset(
                                          english,
                                          fit: BoxFit.cover,
                                        ),
                                  onTap: () {
                                    bottomSheet(context);
                                  },
                                  splashColor: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: width * .05,
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: height * .08,
                      left: width / 5.2,
                      width: width * .3,
                      child: ClipOval(
                        child: Container(
                          width: width * .26,
                          height: width * .3,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                profile_avataar,
                              ),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: height * .03,
                      left: width / 5.3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            _driverProvider.driver.firstName +
                                " " +
                                _driverProvider.driver.lastName,
                            maxLines: 1,
                            style: nameTextStyle.copyWith(
                              fontSize: width * .045,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            _driverProvider.driver.address,
                            maxLines: 1,
                            overflow: TextOverflow.fade,
                            style: nameTextStyle.copyWith(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () async {
                  _dataProvider.setIndex(0);

                  _navigationService.navigateTo(AppRoute.mainScreen);
                },
                child: Container(
                  decoration: _dataProvider.screenIndex == 0
                      ? BoxDecoration(
                          color: Colors.black,
                          boxShadow: [
                            BoxShadow(
                              color: lightBlue.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        )
                      : BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                        ),
                        child: _dataProvider.screenIndex == 0
                            ? Container(
                                width: width * .015,
                                height: height * .06,
                                decoration: BoxDecoration(
                                  color: _dataProvider.screenIndex == 0
                                      ? Colors.white
                                      : Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(
                                      10,
                                    ),
                                    bottomRight: Radius.circular(
                                      10,
                                    ),
                                  ),
                                ),
                              )
                            : SizedBox(
                                width: width * .015,
                                height: height * .06,
                              ),
                      ),
                      SizedBox(
                        width: width * .03,
                      ),
                      Icon(
                        Icons.home,
                        size: width * .06,
                        color: _dataProvider.screenIndex == 0
                            ? Colors.white
                            : Colors.black,
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                      Text(
                        // S.of(context).home,
                        getTranslated(context, 'home'),
                        style: tripStatusTextStyle.copyWith(
                          fontSize: width * .038,
                          color: _dataProvider.screenIndex == 0
                              ? Colors.white
                              : Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () async {
                  _dataProvider.setIndex(1);
                  // Provider.of<DriverProvider>(context, listen: false)
                  //     .updateCurrentStatus("Not Available");
                  _driverProvider.logout();
                  _navigationService.navigateTo(AppRoute.recentTripScreen);
                },
                child: Container(
                  decoration: _dataProvider.screenIndex == 1
                      ? BoxDecoration(
                          color: Colors.black,
                          boxShadow: [
                            BoxShadow(
                              color: lightBlue.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        )
                      : BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                        ),
                        child: _dataProvider.screenIndex == 1
                            ? Container(
                                width: width * .015,
                                height: height * .06,
                                decoration: BoxDecoration(
                                  color: _dataProvider.screenIndex == 1
                                      ? Colors.white
                                      : Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(
                                      10,
                                    ),
                                    bottomRight: Radius.circular(
                                      10,
                                    ),
                                  ),
                                ),
                              )
                            : SizedBox(
                                width: width * .015,
                                height: height * .06,
                              ),
                      ),
                      SizedBox(
                        width: width * .03,
                      ),
                      Icon(
                        Icons.trip_origin,
                        size: width * .06,
                        color: _dataProvider.screenIndex == 1
                            ? Colors.white
                            : Colors.black,
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                      Text(
                        // S.of(context).recentTrip,
                        getTranslated(context, 'recentTrip'),
                        style: tripStatusTextStyle.copyWith(
                          fontSize: width * .038,
                          color: _dataProvider.screenIndex == 1
                              ? Colors.white
                              : Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () async {
                  _dataProvider.setIndex(0);

                  _driverProvider.logout();
                  _navigationService.navigateTo(AppRoute.previousHistoryScreen);
                },
                child: Container(
                  decoration: _dataProvider.screenIndex == 2
                      ? BoxDecoration(
                          color: Colors.black,
                          boxShadow: [
                            BoxShadow(
                              color: lightBlue.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        )
                      : BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                        ),
                        child: _dataProvider.screenIndex == 2
                            ? Container(
                                width: width * .015,
                                height: height * .06,
                                decoration: BoxDecoration(
                                  color: _dataProvider.screenIndex == 2
                                      ? Colors.white
                                      : Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(
                                      10,
                                    ),
                                    bottomRight: Radius.circular(
                                      10,
                                    ),
                                  ),
                                ),
                              )
                            : SizedBox(
                                width: width * .015,
                                height: height * .06,
                              ),
                      ),
                      SizedBox(
                        width: width * .03,
                      ),
                      Icon(
                        Icons.history,
                        size: width * .06,
                        color: _dataProvider.screenIndex == 2
                            ? Colors.white
                            : Colors.black,
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                      Text(
                        // S.of(context).previousHistory,
                        getTranslated(context, 'previousHistory'),
                        style: tripStatusTextStyle.copyWith(
                          fontSize: width * .038,
                          color: _dataProvider.screenIndex == 2
                              ? Colors.white
                              : Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () async {
                  _dataProvider.setIndex(3);

                  _navigationService.navigateTo(AppRoute.updatePasswordScreen);
                  _driverProvider.logout();
                },
                child: Container(
                  decoration: _dataProvider.screenIndex == 3
                      ? BoxDecoration(
                          color: Colors.black,
                          boxShadow: [
                            BoxShadow(
                              color: lightBlue.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        )
                      : BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                        ),
                        child: _dataProvider.screenIndex == 3
                            ? Container(
                                width: width * .015,
                                height: height * .06,
                                decoration: BoxDecoration(
                                  color: _dataProvider.screenIndex == 3
                                      ? Colors.white
                                      : Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(
                                      10,
                                    ),
                                    bottomRight: Radius.circular(
                                      10,
                                    ),
                                  ),
                                ),
                              )
                            : SizedBox(
                                width: width * .015,
                                height: height * .06,
                              ),
                      ),
                      SizedBox(
                        width: width * .03,
                      ),
                      Icon(
                        Icons.lock,
                        size: width * .06,
                        color: _dataProvider.screenIndex == 3
                            ? Colors.white
                            : Colors.black,
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                      Text(
                        // S.of(context).changePassword,
                        getTranslated(context, 'changePassword'),

                        style: tripStatusTextStyle.copyWith(
                          fontSize: width * .038,
                          color: _dataProvider.screenIndex == 3
                              ? Colors.white
                              : Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () async {
                  _dataProvider.setIndex(4);
                  _driverProvider.logout();
                  _navigationService.navigateTo(AppRoute.contactUsScreen);
                },
                child: Container(
                  decoration: _dataProvider.screenIndex == 4
                      ? BoxDecoration(
                          color: Colors.black,
                          boxShadow: [
                            BoxShadow(
                              color: lightBlue.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        )
                      : BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                        ),
                        child: _dataProvider.screenIndex == 4
                            ? Container(
                                width: width * .015,
                                height: height * .06,
                                decoration: BoxDecoration(
                                  color: _dataProvider.screenIndex == 4
                                      ? Colors.white
                                      : Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(
                                      10,
                                    ),
                                    bottomRight: Radius.circular(
                                      10,
                                    ),
                                  ),
                                ))
                            : SizedBox(
                                width: width * .015,
                                height: height * .06,
                              ),
                      ),
                      SizedBox(
                        width: width * .03,
                      ),
                      Icon(
                        Icons.call,
                        size: width * .06,
                        color: _dataProvider.screenIndex == 4
                            ? Colors.white
                            : Colors.black,
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                      Text(
                        // S.of(context).contactUs,
                        getTranslated(context, 'contactUs'),

                        style: tripStatusTextStyle.copyWith(
                          fontSize: width * .038,
                          color: _dataProvider.screenIndex == 4
                              ? Colors.white
                              : Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // InkWell(
              //   onTap: () {
              //     _dataProvider.setIndex(5);
              //     _driverProvider.logout();
              //     print(_dataProvider.language);
              //     // if (_dataProvider.language == 'English') {
              //     //   changeLanguage('ar', context);
              //     // } else if (_dataProvider.language == 'Arabic') {
              //     //   changeLanguage('en', context);
              //     // }

              //     switch (_dataProvider.language) {
              //       case 'العربية':
              //         changeLanguage('ar', context);
              //         _dataProvider.changeLanguage('English');
              //         break;
              //       case 'English':
              //         changeLanguage('en', context);
              //         _dataProvider.changeLanguage('العربية');
              //         break;
              //       default:
              //         changeLanguage('en', context);
              //         _dataProvider.changeLanguage('English');
              //         break;
              //     }
              //     _dataProvider.checkSelectedLanguage();

              //     // changeLanguage('en', context);
              //   },
              //   child: Container(
              //     decoration: _dataProvider.screenIndex == 5
              //         ? BoxDecoration(
              //             color: Colors.black,
              //             boxShadow: [
              //               BoxShadow(
              //                 color: lightBlue.withOpacity(0.5),
              //                 spreadRadius: 5,
              //                 blurRadius: 7,
              //                 offset: Offset(0, 3),
              //               ),
              //             ],
              //             borderRadius: BorderRadius.only(
              //               topLeft: Radius.circular(10),
              //               topRight: Radius.circular(10),
              //               bottomLeft: Radius.circular(10),
              //               bottomRight: Radius.circular(10),
              //             ),
              //           )
              //         : BoxDecoration(),
              //     child: Row(
              //       mainAxisAlignment: MainAxisAlignment.start,
              //       children: [
              //         Padding(
              //           padding: const EdgeInsets.symmetric(
              //             vertical: 2,
              //           ),
              //           child: _dataProvider.screenIndex == 5
              //               ? Container(
              //                   width: width * .015,
              //                   height: height * .06,
              //                   decoration: BoxDecoration(
              //                     color: _dataProvider.screenIndex == 5
              //                         ? Colors.white
              //                         : Colors.black,
              //                     borderRadius: BorderRadius.only(
              //                       topRight: Radius.circular(
              //                         10,
              //                       ),
              //                       bottomRight: Radius.circular(
              //                         10,
              //                       ),
              //                     ),
              //                   ),
              //                 )
              //               : SizedBox(
              //                   width: width * .015,
              //                   height: height * .06,
              //                 ),
              //         ),
              //         SizedBox(
              //           width: width * .03,
              //         ),
              //         Icon(
              //           Icons.language,
              //           size: width * .06,
              //           color: _dataProvider.screenIndex == 5
              //               ? Colors.white
              //               : Colors.black,
              //         ),
              //         SizedBox(
              //           width: width * .05,
              //         ),
              //         Text(
              //           // S.of(context).changePassword,
              //           // getTranslated(context, 'changePassword'),
              //           _dataProvider.language,
              //           style: tripStatusTextStyle.copyWith(
              //             fontSize: width * .038,
              //             color: _dataProvider.screenIndex == 5
              //                 ? Colors.white
              //                 : Colors.black,
              //           ),
              //         ),
              //       ],
              //     ),
              //   ),
              // ),
              InkWell(
                onTap: () {
                  _driverProvider.driverResponse(
                    response: 'REJECTED',
                    context: context,
                  );
                  _driverProvider.checkCurrentTripStatus('No Trip');
                  _driverProvider.changeDriverStatus('Available');
                  _dataProvider.setIndex(6);
                  _dataProvider.showEndDrawerEnabled(false);
                  //  _locationProvider.changeGoLive(false, context);
                  _driverProvider.clear();
                  Provider.of<DriverProvider>(context, listen: false)
                      .updateCurrentStatus("Not Available");
                  _dataProvider.changeDriverStatus(status: "");
                  clearPrefs();
                  Future.delayed(Duration(milliseconds: 400), () {
                    _dataProvider.setIndex(0);
                  });

                  _navigationService
                      .navigateAndRemoveUntil(AppRoute.loginScreen);
                },
                child: Container(
                  decoration: _dataProvider.screenIndex == 6
                      ? BoxDecoration(
                          color: Colors.black,
                          boxShadow: [
                            BoxShadow(
                              color: lightBlue.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                        )
                      : BoxDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                        ),
                        child: _dataProvider.screenIndex == 6
                            ? Container(
                                width: width * .015,
                                height: height * .06,
                                decoration: BoxDecoration(
                                  color: _dataProvider.screenIndex == 6
                                      ? Colors.white
                                      : Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(
                                      10,
                                    ),
                                    bottomRight: Radius.circular(
                                      10,
                                    ),
                                  ),
                                ),
                              )
                            : SizedBox(
                                width: width * .015,
                                height: height * .06,
                              ),
                      ),
                      SizedBox(
                        width: width * .03,
                      ),
                      Icon(
                        Icons.exit_to_app,
                        size: width * .06,
                        color: _dataProvider.screenIndex == 6
                            ? Colors.white
                            : Colors.black,
                      ),
                      SizedBox(
                        width: width * .05,
                      ),
                      Text(
                        // S.of(context).logout,
                        getTranslated(context, 'logout'),

                        style: tripStatusTextStyle.copyWith(
                          fontSize: width * .038,
                          color: _dataProvider.screenIndex == 6
                              ? Colors.white
                              : Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
