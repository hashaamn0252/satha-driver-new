import 'package:SathaCaptain/utilis/styles.dart';
import 'package:flutter/material.dart';

class CustomRaisedButton extends StatelessWidget {
  final String title;
  final VoidCallback callback;

  const CustomRaisedButton({
    Key key,
    @required this.title,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      height: height * .12,
      decoration: BoxDecoration(
        color: Color(0xffD4D5DA),
        borderRadius: BorderRadius.circular(
          60,
        ),
      ),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          width: width,
          height: width,
          child: Center(
            child: Text(
              title,
              style: tripStatusTextStyle.copyWith(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
          ),
        ),
        color: Colors.black87,
        onPressed: () {
          callback();
        },
      ),
    );
  }
}
