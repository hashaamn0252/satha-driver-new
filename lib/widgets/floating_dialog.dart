import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../language_constants.dart';

class FloatingDialog extends StatefulWidget {
  const FloatingDialog({
    Key key,
    this.openEndDrawer,
  }) : super(key: key);
  final VoidCallback openEndDrawer;

  @override
  _FloatingDialogState createState() => _FloatingDialogState();
}

class _FloatingDialogState extends State<FloatingDialog> {
  var _navigationService = locator<NavigationService>();
  Color color = Colors.red;
  String status = '';
  String tripStatus = '';
  Widget cupertino = SizedBox();
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var _locationProvider = Provider.of<MapBoxLocationProvider>(context);
    var _dataProvider = Provider.of<DataProvider>(context);
    var _userProvider = Provider.of<DriverProvider>(context);
    print(_userProvider.currentTripStatus);
    if (_userProvider.currentTripStatus == null) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');

        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          PushNotificationService pushNotificationService =
              PushNotificationService();
          pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              // status = S.of(context).online;
              status = getTranslated(context, 'online');

              color = Colors.green;
              tripStatus = getTranslated(context, 'waitingForRide');
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              // status = S.of(context).offline;
              status = getTranslated(context, 'offline');
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'No Trip'.toLowerCase()) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');
        color = Colors.green;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          PushNotificationService pushNotificationService =
              PushNotificationService();
          pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("AVAILABLE");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.isEmpty) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');
        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          PushNotificationService pushNotificationService =
              PushNotificationService();
          pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'On Ride'.toLowerCase()) {
      // status = S.of(context).busy;
      status = getTranslated(context, 'busy');
      tripStatus = '';
      _dataProvider.changeWidget(SizedBox());
      color = Colors.red;
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'Available'.toLowerCase()) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        status = 'Online';
        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          PushNotificationService pushNotificationService =
              PushNotificationService();
          pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else {
      // status = S.of(context).busy;
      status = getTranslated(context, 'busy');
      tripStatus = '';
      _dataProvider.changeWidget(SizedBox());
      color = Colors.red;
    }

    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.transparent,
        image: DecorationImage(
          image: AssetImage(
            floating_dialog_background,
          ),
          fit: BoxFit.fill,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: Row(
          children: [
            SizedBox(
                width: width * .2,
                height: width * .2,
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage(
                    profile_avataar,
                  ),
                )),
            SizedBox(
              width: width * .02,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      // S.of(context).driver,
                      getTranslated(context, 'driver'),
                      style: tripStatusTextStyle.copyWith(
                        fontSize: 22,
                        color: greyColorDark,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: width * .15,
                    ),
                    _dataProvider.cupertino,
                  ],
                ),
                Text(
                  status,
                  style: tripStatusTextStyle.copyWith(
                    fontSize: 14,
                    color: color,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  tripStatus,
                  style: tripStatusTextStyle,
                ),
                SizedBox(
                  height: height * .02,
                ),
              ],
            ),
            Spacer(),
            SizedBox(
              width: width * .2,
              height: width * .2,
              child: ClipOval(
                child: Material(
                  child: InkWell(
                    onTap: () async {
                      _dataProvider.showEndDrawerEnabled(true);
                      widget.openEndDrawer();
                      PushNotificationService pushNotificationService =
                          PushNotificationService();
                      _userProvider.logout();
                    },
                    splashColor: Colors.black,
                    child: SizedBox(
                      child: Image.asset(
                        menu_icon2,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
