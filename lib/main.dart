import 'dart:io';

import 'package:SathaCaptain/bloc/basehelper.dart';
import 'package:SathaCaptain/language_constants.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/constants.dart';
import 'package:SathaCaptain/utilis/device_info.dart';
import 'package:SathaCaptain/utilis/globals.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'appLocalizatin.dart';
import 'app_router.dart';
import 'bloc/connectivity.dart';

import 'locator.dart';
import 'package:SathaCaptain/utilis/globals.dart' as globals;

import 'model/singleton.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  setupLocator();

  runApp(
    MultiProvider(
      child: MyApp(),
      providers: [
        ChangeNotifierProvider<DataProvider>(
          create: (_) => DataProvider(),
          lazy: false,
        ),
        ChangeNotifierProvider<DriverProvider>(
          create: (_) => DriverProvider(),
          lazy: false,
        ),
        ChangeNotifierProvider<MapBoxLocationProvider>(
          create: (_) => MapBoxLocationProvider(),
          lazy: false,
        ),
      ],
    ),
  );
}

class MyApp extends StatefulWidget {
  static void setLocale(
    BuildContext context,
    Locale newLocale,
  ) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(newLocale);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  SystemInfo systemInfo = SystemInfo();

  AppLifecycleState appLifecycleState;
  static const platform = const MethodChannel('com.brightit.brightqtr');
  //APPLIFECYCLESTATE INDEX (1 => RESUME 2=> ACTIVE 0 => INACTIVE)

  List names = [];

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    // var _dataProvider = Provider.of<DataProvider>(context, listen: false);
    print("state:./........... $state");
    super.didChangeAppLifecycleState(state);
    // state == AppLifecycleState.inactive ||
    if (state == AppLifecycleState.detached) {
      // driverStatusChangeWhenAppKill();
//       var appDir = (await getTemporaryDirectory()).path;
// new Directory(appDir).delete(recursive: true);
      // APIHELPER().offlineUser(context);

      // String driverId = await getUserIdPrefs() ?? '';

      print("heasd");
      // if (Provider.of<DriverProvider>(context, listen: false)
      //         .driverStatus
      //         .toLowerCase() ==
      //     'Available'.toLowerCase()) {
      //   Provider.of<MapBoxLocationProvider>(context, listen: false)
      //       .changeGoLive(false, context);
      //   globals.driverStatusChangeWhenAppKill();
      // }
    }
  }

  @override
  void deactivate() {
    // Provider.of<DriverProvider>(context, listen: false)
    //     .updateCurrentStatus("Not Available");
    // APIHELPER().offlineUser(context);
    // globals.driverStatusChangeWhenAppKill();
    super.deactivate();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    globals.initalisePushNotifications(context: context);
  }

  Locale _locale;
  setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void didChangeDependencies() {
    getLocale().then((locale) {
      setState(() {
        this._locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  Future<void> dispose() {
    if (Userss.userData.driverId != null) {
      print("driver id: ${Userss.userData.driverId}");
      // APIHELPER().offlineUser(context);
      // var data = {
      //   "DriverId": Userss.userData.driverId,
      //   "Status": "Not Available"
      // };
      // print(data);
      // Future.delayed(Duration(milliseconds: 500), () async {
      //   await http.post(updateStatusURL, headers: webAPIKey, body: data);
      // });

    } else {
      // print("driver id: ${Userss.userData.driverId}");
    }
    // Provider.of<DriverProvider>(context, listen: false)
    //     .updateCurrentStatus("Not Available");
    // globals.driverStatusChangeWhenAppKill();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    globals.myBloc = BlocClass(true);
    return StreamBuilder<bool>(
      builder: (context, snapshot) {
        Provider.of<MapBoxLocationProvider>(context, listen: false)
            .getUserLocation();
        Provider.of<DataProvider>(context, listen: false)
            .checkSelectedLanguage();

        return WillPopScope(
          // ignore: missing_return
          onWillPop: () {
            // Provider.of<DriverProvider>(context, listen: false)
            //     .updateCurrentStatus("Not Available");
            if (Platform.isIOS) {
              try {
                exit(0);
              } catch (e) {
                SystemNavigator
                    .pop(); // for IOS, not true this, you can make comment this :)
              }
            } else {
              try {
                SystemNavigator.pop(); // sometimes it cant exit app
              } catch (e) {
                exit(0); // so i am giving crash to app ... sad :(
              }
            }
          },
          child: MaterialApp(
            title: 'Satha Driver',
            builder: BotToastInit(),
            navigatorObservers: [BotToastNavigatorObserver()],
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primaryColor: greyColorDark,
              accentColor: Colors.white,
              primarySwatch: pink,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              cursorColor: greyColorDark,
              dialogTheme: DialogTheme(
                titleTextStyle: tripStatusTextStyle.copyWith(
                  color: greyColorDark,
                ),
                contentTextStyle: tripStatusTextStyle.copyWith(
                  color: greyColorDark,
                ),
              ),
            ),
            locale: _locale,
            onGenerateRoute: AppRoute.generateRoute,
            localeResolutionCallback: (locale, supportedLocales) {
              for (var supportedLocale in supportedLocales) {
                if (supportedLocale.languageCode == locale.languageCode &&
                    supportedLocale.countryCode == locale.countryCode) {
                  return supportedLocale;
                }
              }
              return supportedLocales.first;
            },
            initialRoute: AppRoute.splashScreen,
            navigatorKey: locator<NavigationService>().navigatorKey,
            localizationsDelegates: [
              AppLocalization.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: [
              Locale("en", "US"),
              Locale("ar", "SA"),
            ],
          ),
        );
      },
    );
  }
}
