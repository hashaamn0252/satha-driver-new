import 'package:SathaCaptain/language_constants.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/screens/phone_number_screen.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/routes.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:SathaCaptain/widgets/custom_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var _navigationService = locator<NavigationService>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var _formKey = GlobalKey<FormState>();
  String mobileNumber = '', password = '';
  TextEditingController mobile = TextEditingController();
  TextEditingController pass = TextEditingController();

  @override
  void dispose() {
    Provider.of<DriverProvider>(context, listen: false)
        .passwordTextEditingController
        .dispose();
    Provider.of<DriverProvider>(context, listen: false)
        .userNameTextEditingController
        .dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _dataProvider = Provider.of<DataProvider>(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _userProvider = Provider.of<DriverProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: greyColor,
      body: SafeArea(
        child: SingleChildScrollView(
          reverse: true,
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: height * .01,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: width * .15,
                      child: ClipOval(
                        child: Material(
                          child: InkWell(
                            child: _dataProvider.language == "العربية"
                                ? Image.asset(
                                    arabic,
                                    fit: BoxFit.cover,
                                  )
                                : Image.asset(
                                    english,
                                    fit: BoxFit.cover,
                                  ),
                            onTap: () {
                              bottomSheet(context);
                            },
                            splashColor: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: width * .05,
                    ),
                  ],
                ),
                SizedBox(
                    width: width * .7,
                    child: Image.asset(
                      sathaLogo,
                    )),
                SizedBox(
                  height: height * .05,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: width * .15,
                      height: width * .15,
                      decoration: BoxDecoration(
                        color: Colors.grey[350],
                        borderRadius: BorderRadius.circular(
                          10,
                        ),
                      ),
                      child: Center(
                        child: Text(
                          '+974',
                          style: tripStatusTextStyle.copyWith(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: width * .02,
                    ),
                    Container(
                      width: width * .6,
                      child: TextFormField(
                        controller: mobile,
                        // textDirection: TextDirection.ltr,
                        decoration: InputDecoration(
                          hintText: getTranslated(context, 'phoneNumber'),
                          hintStyle: tripStatusTextStyle.copyWith(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                        onSaved: (newValue) {
                          mobileNumber = newValue;
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return getTranslated(
                                context, 'enterYourPhoneNumber');
                          }
                          if (value.length < 8) {
                            return getTranslated(
                                context, 'enterYourFullPhoneNumber');
                          }
                        },
                        maxLength: 8,
                        keyboardType: TextInputType.phone,
                        style: tripStatusTextStyle.copyWith(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .01,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: width * .15,
                      height: width * .15,
                      decoration: BoxDecoration(
                        color: Colors.grey[350],
                        borderRadius: BorderRadius.circular(
                          10,
                        ),
                      ),
                      child: Center(
                          child: Icon(
                        Icons.lock,
                        color: Colors.black,
                      )),
                    ),
                    SizedBox(
                      width: width * .02,
                    ),
                    Container(
                      width: width * .6,
                      child: TextFormField(
                        controller: pass,
                        decoration: InputDecoration(
                          hintText: getTranslated(context, 'password'),

                          // S.of(context).password,
                          hintStyle: tripStatusTextStyle.copyWith(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            // return S.of(context).enterPassword;
                            return getTranslated(context, 'enterPassword');
                          }
                          if (value.length < 8)
                            // return S.of(context).passwordLength;
                            return getTranslated(context, 'passwordLength');
                        },
                        obscureText: true,
                        style: tripStatusTextStyle.copyWith(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .08,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 50,
                  ),
                  child: FlatButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _userProvider.signIn(
                          mobile: mobile.text,
                          password: pass.text,
                          context: context,
                        );
                      }
                      // RestartWidget.restartApp(context);
                    },
                    splashColor: Colors.grey,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: width,
                      height: height * .07,
                      child: Center(
                        child: Text(
                          // S.of(context).login,
                          getTranslated(context, 'login'),

                          style: tripStatusTextStyle.copyWith(
                            fontSize: 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    color: greyColorDark,
                  ),
                ),
                SizedBox(
                  height: height * .02,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                  child: InkWell(
                    onTap: () {
                      AppRoutes.push(
                          context,
                          MobileNumberScreen(
                            screen: 0,
                          ));
                    },
                    child: Text(
                      // S.of(context).forgotPassword,
                      getTranslated(context, 'forgotPassword'),

                      style: tripStatusTextStyle.copyWith(
                        color: Colors.blue[600],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: height * .02,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                  child: InkWell(
                    onTap: () {
                      AppRoutes.push(
                          context,
                          MobileNumberScreen(
                            screen: 1,
                          ));
                      // AppRoutes.replace(context, RegistrationScreen());
                    },
                    child: Text(
                      // S.of(context).forgotPassword,
                      getTranslated(context, 'Sign Up'),

                      style: tripStatusTextStyle.copyWith(
                        color: Colors.blue[600],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: height * .02,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
