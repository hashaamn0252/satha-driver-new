import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/screens/update_password_screen.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/routes.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

import '../app_router.dart';
import '../language_constants.dart';

class OTPVeritifcationScreen extends StatefulWidget {
  OTPVeritifcationScreen({Key key}) : super(key: key);

  @override
  _OTPVeritifcationScreenState createState() => _OTPVeritifcationScreenState();
}

class _OTPVeritifcationScreenState extends State<OTPVeritifcationScreen> {
  var _navigationService = locator<NavigationService>();
  var _userProvider;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var _formKey = GlobalKey<FormState>();
  var otpp;
  Size _screenSize;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    Provider.of<DriverProvider>(context, listen: false)
        .otpTextEditingController
        .dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    _userProvider = Provider.of<DriverProvider>(context);
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: greyColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            size: 32,
            color: Colors.black,
          ),
        ),
      ),
      extendBody: true,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox(
                height: height * .05,
              ),
              // SizedBox(
              //     width: width * .7,
              //     child: Image.asset(
              //       sathaLogo,
              //     )),

              Text(
                // S.of(context).verification,
                getTranslated(context, 'verification'),

                style: nameTextStyle.copyWith(
                  color: Colors.black87,
                ),
              ),
              SizedBox(
                height: height * .01,
              ),
              Text(
                // S.of(context).verificationCode
                getTranslated(context, 'verificationCode'),
              ),
              SizedBox(
                height: height * .1,
              ),
              _getInputField,
              SizedBox(
                height: height * .06,
              ),
              _getOtpKeyboard
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   textDirection: TextDirection.ltr,
              //   children: [
              //     Container(
              //       width: width * .6,
              //       child: PinCodeTextField(
              //         controller: _userProvider.otpTextEditingController,
              //         appContext: context,
              //         length: 4,
              //         validator: (value) {
              //           if (value.isEmpty) {
              //             // return S.of(context).pinCodeText;
              //             return getTranslated(context, 'pinCodeText');
              //           }
              //           if (value.length < 4) {
              //             // return S.of(context).pinCodeText;
              //             return getTranslated(context, 'pinCodeText');
              //           }
              //         },
              //         onChanged: (_) {},
              //         backgroundColor: Colors.transparent,
              //         keyboardType: TextInputType.number,
              //         pinTheme: PinTheme.defaults(
              //           activeColor: Colors.black,
              //           inactiveColor: Colors.grey,
              //           selectedColor: Colors.black,
              //         ),
              //       ),
              //     ),
              //   ],
              // ),
              // SizedBox(
              //   height: height * .03,
              // ),
              ,
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 70,
                ),
                child: FlatButton(
                  splashColor: Colors.grey,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      if (otpp == _userProvider.otpCode.toString()) {
                        _userProvider.clear();
                        AppRoutes.replace(context, UpdatePasswordScreen());
                        // _navigationService.navigateAndRemoveUntil(
                        //     AppRoute.updatePasswordScreen);
                      } else {
                        BotToast.showText(
                            text: 'You have entered wrong OTP Code.');
                      }
                    }
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    width: width,
                    height: height * .07,
                    child: Center(
                      child: Text(
                        // S.of(context).verify.toUpperCase(),
                        getTranslated(context, 'verify').toUpperCase(),

                        style: tripStatusTextStyle.copyWith(
                          fontSize: 22,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  color: greyColorDark,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      textDirection: TextDirection.ltr,
      children: <Widget>[
        _otpTextField(_firstDigit),
        _otpTextField(_secondDigit),
        _otpTextField(_thirdDigit),
        _otpTextField(_fourthDigit),
      ],
    );
  }

  get _getOtpKeyboard {
    return new Container(
        height: _screenSize.width - 60,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new SizedBox(
                    width: 80.0,
                  ),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        setState(() {
                          if (_fourthDigit != null) {
                            _fourthDigit = null;
                          } else if (_thirdDigit != null) {
                            _thirdDigit = null;
                          } else if (_secondDigit != null) {
                            _secondDigit = null;
                          } else if (_firstDigit != null) {
                            _firstDigit = null;
                          }
                        });
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  Widget _otpTextField(int digit) {
    return new Container(
      width: 35.0,
      height: 45.0,
      alignment: Alignment.center,
      child: new Text(
        digit != null ? digit.toString() : "",
        textDirection: TextDirection.ltr,
        style: new TextStyle(
          fontSize: 30.0,
          color: Colors.black,
        ),
      ),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
        width: 2.0,
        color: Colors.black,
      ))),
    );
  }

  void _setCurrentDigit(int i) {
    setState(() {
      _currentDigit = i;
      if (_firstDigit == null) {
        _firstDigit = _currentDigit;
      } else if (_secondDigit == null) {
        _secondDigit = _currentDigit;
      } else if (_thirdDigit == null) {
        _thirdDigit = _currentDigit;
      } else if (_fourthDigit == null) {
        _fourthDigit = _currentDigit;

        otpp = _firstDigit.toString() +
            _secondDigit.toString() +
            _thirdDigit.toString() +
            _fourthDigit.toString();
      }
    });
  }

  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(40.0),
        child: new Container(
          height: 80.0,
          width: 80.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: new Center(
            child: new Text(
              label,
              style: new TextStyle(
                fontSize: 30.0,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
