import 'package:SathaCaptain/app_router.dart';
import 'package:SathaCaptain/bloc/helper.dart';
import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/model/userdatamodel.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/screens/signupOTP.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/routes.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../language_constants.dart';
import '../locator.dart';

import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class MobileNumberScreen extends StatefulWidget {
  int screen;
  MobileNumberScreen({Key key, this.screen}) : super(key: key);

  @override
  _MobileNumberScreenState createState() => _MobileNumberScreenState();
}

class _MobileNumberScreenState extends State<MobileNumberScreen> {
  var _navigationService = locator<NavigationService>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController pinCode = TextEditingController();
  bool otpVerify = false;
  var _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    Provider.of<DriverProvider>(context, listen: false)
        .phoneNumberTextEditingController
        .dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _userProvider = Provider.of<DriverProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: greyColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: InkWell(
          onTap: () {
            _navigationService.goBack();
          },
          child: Icon(
            Icons.arrow_back,
            size: 32,
            color: Colors.black,
          ),
        ),
      ),
      extendBody: true,
      body: SafeArea(
        child: SingleChildScrollView(
          reverse: true,
          physics: BouncingScrollPhysics(),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: height * .05,
                ),
                SizedBox(
                    width: width * .7,
                    child: Image.asset(
                      sathaLogo,
                    )),
                SizedBox(
                  height: height * .05,
                ),
                Text(
                  // S.of(context).phoneNumber,
                  getTranslated(context, 'phoneNumber'),

                  style: nameTextStyle.copyWith(
                    color: Colors.black87,
                  ),
                ),
                SizedBox(
                  height: height * .1,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: width * .15,
                      height: width * .15,
                      decoration: BoxDecoration(
                        color: Colors.grey[350],
                        borderRadius: BorderRadius.circular(
                          10,
                        ),
                      ),
                      child: Center(
                        child: Text(
                          '+974',
                          style: tripStatusTextStyle.copyWith(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: width * .02,
                    ),
                    Container(
                      width: width * .5,
                      child: TextFormField(
                        controller:
                            _userProvider.phoneNumberTextEditingController,
                        decoration: InputDecoration(
                          hintText:
                              // S.of(context).phoneNumber,
                              getTranslated(context, 'phoneNumber'),
                          hintStyle: tripStatusTextStyle.copyWith(
                            color: Colors.black,
                            fontSize: 18,
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            // return S.of(context).fieldEmptyText;
                            return getTranslated(context, 'fieldEmptyText');
                          }
                          if (value.length < 8) {
                            // return S.of(context).enterYourFullPhoneNumber;
                            return getTranslated(
                                context, 'enterYourFullPhoneNumber');
                          }
                        },
                        maxLength: 8,
                        keyboardType: TextInputType.phone,
                        style: tripStatusTextStyle.copyWith(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .03,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 60,
                  ),
                  child: FlatButton(
                    splashColor: Colors.grey,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        if (widget.screen == 1) {
                          setState(() {
                            User.userData.mobile = _userProvider
                                .phoneNumberTextEditingController.text;
                          });
                          BASEHELPER()
                              .sendMobile(
                                  "${_userProvider.phoneNumberTextEditingController.text.trim()}",
                                  context)
                              .then((value) {
                            if (value == null) {
                              BotToast.showText(
                                text: "something went wrong",
                                duration: Duration(seconds: 2),
                              );
                            } else if (value['Status'] == true) {
                              print("sign up response: $value");
                              AppRoutes.replace(
                                  context,
                                  SignUpOTP(
                                    OTP: value['Data']['Pin'],
                                    mobile: User.userData.mobile,
                                  ));
                              BotToast.showText(
                                text: "${value["Message"]['EnResponse']}",
                                duration: Duration(seconds: 2),
                              );
                            } else {
                              BotToast.showText(
                                text: "${value["Message"]['EnResponse']}",
                                duration: Duration(seconds: 2),
                              );
                            }
                          }).catchError((error) {
                            BotToast.showText(
                              text: "$error",
                              duration: Duration(seconds: 2),
                            );
                          });
                        } else {
                          _userProvider.forgotPassword(
                            context,
                            _userProvider.phoneNumberTextEditingController.text
                                .trim(),
                            widget.screen,
                          );
                          // _userProvider.clear();
                        }
                      }
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: width,
                      height: height * .07,
                      child: Center(
                        child: Text(
                          // S.of(context).confirm.toUpperCase(),
                          getTranslated(context, 'confirm').toUpperCase(),

                          style: tripStatusTextStyle.copyWith(
                            fontSize: 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    color: greyColorDark,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
