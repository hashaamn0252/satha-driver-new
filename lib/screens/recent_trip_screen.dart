import 'package:SathaCaptain/component/trip_arrived_widget.dart';
import 'package:SathaCaptain/component/trip_details_widget.dart';
import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:SathaCaptain/widgets/custom_drawer.dart';
import 'package:SathaCaptain/widgets/floating_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../language_constants.dart';

class RecentTripScreen extends StatefulWidget {
  RecentTripScreen({Key key}) : super(key: key);

  @override
  _RecentTripScreenState createState() => _RecentTripScreenState();
}

class _RecentTripScreenState extends State<RecentTripScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Color color = Colors.red;
  String status = '';
  String tripStatus = '';
  Widget cupertino = SizedBox();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _dataProvider = Provider.of<DataProvider>(context);
    var _driverProvider = Provider.of<DriverProvider>(context);
    print(_driverProvider.currentTripStatus);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body:
          // TripArrivedWidget(),
          SafeArea(
        child: _driverProvider.currentTripStatus.toLowerCase() ==
                'No Trip'.toLowerCase()
            ? Column(
                children: [
                  Container(
                    height: height * .164,
                    child: dialogs(),
                    // FloatingDialog(
                    //   openEndDrawer: () {
                    //     _scaffoldKey.currentState.openEndDrawer();
                    //     print("working");
                    //   },
                    // ),
                  ),
                  Container(
                    height: height * .78,
                    width: width,
                    child: Stack(
                      children: [
                        SizedBox(
                          height: height * .1,
                        ),
                        Positioned(
                          top: height * .15,
                          left: width * .14,
                          child: Card(
                            color: greyColorDark.withOpacity(
                              0.8,
                            ),
                            elevation: 3,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                width * .03,
                              ),
                            ),
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Container(
                              height: height * .3,
                              width: width * .7,
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: height * .11,
                                  ),
                                  Container(
                                    width: width * .5,
                                    height: height * .08,
                                    decoration: BoxDecoration(
                                      color: Colors.yellow[600],
                                      borderRadius: BorderRadius.circular(
                                        width * .1,
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        'No Recent Trip Found',
                                        style: tripStatusTextStyle.copyWith(
                                          color: Colors.white,
                                          // Colors.black.withOpacity(
                                          //   0.8,
                                          // ),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: height * .28,
                          right: width * .1,
                          child: Container(
                            child: SizedBox(
                              width: width * .4,
                              height: width * .3,
                              child: Image.asset(
                                satha_text,
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          top: height * .09,
                          left: width * .36,
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadiusDirectional.circular(
                                width * .5,
                              ),
                            ),
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            elevation: 10,
                            child: Container(
                              width: width * .25,
                              height: width * .25,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                    hourGlassGif,
                                  ),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    spreadRadius: 12,
                                    blurRadius: 14,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: height * .02,
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : _driverProvider.currentTripStatus == null
                ? Column(
                    children: [
                      Container(
                        height: height * .164,
                        child: dialogs(),
                        // child: FloatingDialog(
                        //   openEndDrawer: () {
                        //     _scaffoldKey.currentState.openEndDrawer();
                        //     print("working");
                        //   },
                        // ),
                      ),
                      Container(
                        height: height * .78,
                        width: width,
                        child: Stack(
                          children: [
                            SizedBox(
                              height: height * .1,
                            ),
                            Positioned(
                              top: height * .15,
                              left: width * .14,
                              child: Card(
                                color: greyColorDark.withOpacity(
                                  0.8,
                                ),
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                    width * .03,
                                  ),
                                ),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Container(
                                  height: height * .3,
                                  width: width * .7,
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: height * .11,
                                      ),
                                      Container(
                                        width: width * .5,
                                        height: height * .08,
                                        decoration: BoxDecoration(
                                          color: Colors.yellow[600],
                                          borderRadius: BorderRadius.circular(
                                            width * .1,
                                          ),
                                        ),
                                        child: Center(
                                          child: Text(
                                            'No Recent Trip Found',
                                            style: tripStatusTextStyle.copyWith(
                                              color: Colors.white,
                                              // Colors.black.withOpacity(
                                              //   0.8,
                                              // ),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: height * .28,
                              right: width * .1,
                              child: Container(
                                child: SizedBox(
                                  width: width * .4,
                                  height: width * .3,
                                  child: Image.asset(
                                    satha_text,
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              top: height * .09,
                              left: width * .36,
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadiusDirectional.circular(
                                    width * .5,
                                  ),
                                ),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                elevation: 10,
                                child: Container(
                                  width: width * .25,
                                  height: width * .25,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(
                                        hourGlassGif,
                                      ),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey,
                                        spreadRadius: 12,
                                        blurRadius: 14,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * .02,
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                : Column(
                    children: [
                      Container(
                        width: width,
                        height: height * .164,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(60),
                            bottomRight: Radius.circular(60),
                          ),
                        ),
                        child: dialogs(),
                        // child: FloatingDialog(
                        //   openEndDrawer: () {
                        //     _scaffoldKey.currentState.openEndDrawer();
                        //   },
                        // ),
                      ),
                      SizedBox(
                        height: height * .03,
                      ),
                      _driverProvider.currentTripStatus == ''
                          ? TripArrivedWidget()
                          : SizedBox(),
                      TripDetailsWidget()
                    ],
                  ),
      ),
      endDrawer: CustomDrawer(),
    );
  }

  Widget dialogs() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var _locationProvider = Provider.of<MapBoxLocationProvider>(context);
    var _dataProvider = Provider.of<DataProvider>(context);
    var _userProvider = Provider.of<DriverProvider>(context);
    print(_userProvider.currentTripStatus);
    if (_userProvider.currentTripStatus == null) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');

        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              // status = S.of(context).online;
              status = getTranslated(context, 'online');

              color = Colors.green;
              tripStatus = getTranslated(context, 'waitingForRide');
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              // status = S.of(context).offline;
              status = getTranslated(context, 'offline');
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'No Trip'.toLowerCase()) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');
        color = Colors.green;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("AVAILABLE");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.isEmpty) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');
        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'On Ride'.toLowerCase()) {
      // status = S.of(context).busy;
      status = getTranslated(context, 'busy');
      tripStatus = '';
      _dataProvider.changeWidget(SizedBox());
      color = Colors.red;
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'Available'.toLowerCase()) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        status = 'Online';
        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else {
      // status = S.of(context).busy;
      status = getTranslated(context, 'busy');
      tripStatus = '';
      _dataProvider.changeWidget(SizedBox());
      color = Colors.red;
    }

    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.transparent,
        image: DecorationImage(
          image: AssetImage(
            floating_dialog_background,
          ),
          fit: BoxFit.fill,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: Row(
          children: [
            SizedBox(
                width: width * .2,
                height: width * .2,
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage(
                    profile_avataar,
                  ),
                )),
            SizedBox(
              width: width * .02,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      // S.of(context).driver,
                      getTranslated(context, 'driver'),
                      style: tripStatusTextStyle.copyWith(
                        fontSize: 22,
                        color: greyColorDark,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: width * .15,
                    ),
                    _dataProvider.cupertino,
                  ],
                ),
                Text(
                  status,
                  style: tripStatusTextStyle.copyWith(
                    fontSize: 14,
                    color: color,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  tripStatus,
                  style: tripStatusTextStyle,
                ),
                SizedBox(
                  height: height * .02,
                ),
              ],
            ),
            Spacer(),
            SizedBox(
              width: width * .2,
              height: width * .2,
              child: ClipOval(
                child: Material(
                  child: InkWell(
                    onTap: () async {
                      _dataProvider.showEndDrawerEnabled(true);
                      _scaffoldKey.currentState.openEndDrawer();
                      // PushNotificationService pushNotificationService =
                      //     PushNotificationService();
                      // _userProvider.logout();
                    },
                    splashColor: Colors.black,
                    child: SizedBox(
                      child: Image.asset(
                        menu_icon2,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
