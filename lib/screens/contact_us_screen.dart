import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:flutter/material.dart';

import '../language_constants.dart';

class ContactUsScreen extends StatelessWidget {
  ContactUsScreen({Key key}) : super(key: key);
  var _navigationService = locator<NavigationService>();

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: greyColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: InkWell(
          onTap: () {
            _navigationService.goBack();
          },
          child: Icon(
            Icons.arrow_back,
            size: 32,
            color: Colors.black,
          ),
        ),
      ),
      extendBody: true,
      body: SafeArea(
        child: SingleChildScrollView(
          reverse: true,
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(
                height: height * .05,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.email,
                    size: 140,
                    color: Colors.black87,
                  ),
                ],
              ),
              Text(
                // S.of(context).contactUs,
                getTranslated(context, 'contactUs'),

                style: nameTextStyle.copyWith(
                  color: Colors.black87,
                ),
              ),
              SizedBox(
                height: height * .12,
              ),
              Container(
                width: width * .6,
                height: width * .1,
                decoration: BoxDecoration(
                  color: Colors.grey[350],
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      width: width * .05,
                    ),
                    Icon(
                      Icons.email,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: width * .01,
                    ),
                    Text(
                      'info@sathaqtr.com',
                      style: tripStatusTextStyle.copyWith(
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      width: width * .05,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
