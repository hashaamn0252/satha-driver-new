import 'package:SathaCaptain/bloc/helper.dart';
import 'package:SathaCaptain/language_constants.dart';
import 'package:SathaCaptain/screens/signup.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/routes.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import '../locator.dart';
import 'package:SathaCaptain/locator.dart';

import 'package:flutter/cupertino.dart';

import 'package:flutter/services.dart';

import '../app_router.dart';

class SetNewPasswordScreen extends StatelessWidget {
  TextEditingController _passwordTextEditingController =
      TextEditingController();

  final mobileNumber, otp;

  var _formKey = GlobalKey<FormState>();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  SetNewPasswordScreen(
      {Key key, @required this.mobileNumber, @required this.otp})
      : super(key: key);
  NavigationService _navigationService = locator<NavigationService>();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: greyColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          "${getTranslated(context, "profile")}",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        leading: InkWell(
          onTap: () {
            _navigationService.goBack();

            // navigateReplace(
            //   AppRoute.setprofiles,
            // );
          },
          child: Icon(
            Icons.arrow_back,
            size: 32,
            color: Colors.black,
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              width: width,
              height: height,
              child: Form(
                key: _formKey,
                child: Column(
                  //  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: height * .1,
                    ),
                    SizedBox(
                        width: width * .7,
                        child: Image.asset(
                          sathaLogo,
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Container(
                        //   width: width * .10,
                        //   height: width * .10,
                        //   decoration: BoxDecoration(
                        //     color: Colors.grey, //Colors.grey[350],
                        //     borderRadius: BorderRadius.circular(
                        //       10,
                        //     ),
                        //   ),
                        //   child: Center(
                        //       child: Icon(

                        //     color: Colors.black, //Colors.grey,
                        //   )),
                        // ),
                        Container(
                          width: width * .15,
                          height: width * .15,
                          decoration: BoxDecoration(
                            color: Colors.grey[350],
                            borderRadius: BorderRadius.circular(
                              10,
                            ),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.person_rounded,
                            color: Colors.black,
                          )),
                        ),
                        SizedBox(
                          width: width * .02,
                        ),
                        Container(
                          width: width * .6,
                          child: TextFormField(
                            controller: firstName,
                            decoration: InputDecoration(
                              hintText:
                                  '${getTranslated(context, "first_name")}',
                              hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                            // onSaved: (newValue) {
                            //   mobileNumber = newValue;
                            // },
                            validator: (value) {
                              if (value.isEmpty) {
                                return "${getTranslated(context, "enter_first_name")}";
                              }
                              // if (value.length < 8) {
                              //   return 'Please enter full number';
                              // }
                            },
                            //maxLength: 8,
                            keyboardType: TextInputType.name,
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Container(
                        //   width: width * .10,
                        //   height: width * .10,
                        //   decoration: BoxDecoration(
                        //     color: Colors.grey,

                        //     ///Colors.grey[350],
                        //     borderRadius: BorderRadius.circular(
                        //       10,
                        //     ),
                        //   ),
                        //   child: Center(
                        //       child: Icon(
                        //           color: Colors.black //Colors.grey,
                        //           )),
                        //   // child: Center(
                        //   //   child: Text(
                        //   //     '+974',
                        //   //     style: tripStatusTextStyle.copyWith(
                        //   //       color: Theme.of(context).accentColor,//Colors.grey,
                        //   //       fontSize: 18,
                        //   //     ),
                        //   //   ),
                        //   // ),
                        // ),
                        Container(
                          width: width * .15,
                          height: width * .15,
                          decoration: BoxDecoration(
                            color: Colors.grey[350],
                            borderRadius: BorderRadius.circular(
                              10,
                            ),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.person_rounded,
                            color: Colors.black,
                          )),
                        ),
                        SizedBox(
                          width: width * .02,
                        ),
                        Container(
                          width: width * .6,
                          child: TextFormField(
                            controller: lastName,
                            decoration: InputDecoration(
                              hintText:
                                  '${getTranslated(context, "last_name")}',
                              hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                            // onSaved: (newValue) {
                            //   mobileNumber = newValue;
                            // },
                            validator: (value) {
                              if (value.isEmpty) {
                                return "${getTranslated(context, "enter_last_name")}";
                              }
                              // if (value.length < 8) {
                              //   return 'Please enter full number';
                              // }
                            },
                            //maxLength: 8,
                            keyboardType: TextInputType.name,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height * .02,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: width * .15,
                          height: width * .15,
                          decoration: BoxDecoration(
                            color: Colors.grey[350],
                            borderRadius: BorderRadius.circular(
                              10,
                            ),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.lock,
                            color: Colors.black,
                          )),
                        ),
                        // Container(
                        //   width: width * .10,
                        //   height: width * .10,
                        //   decoration: BoxDecoration(
                        //     color: Colors.grey, //Colors.grey[350],
                        //     borderRadius: BorderRadius.circular(
                        //       10,
                        //     ),
                        //   ),
                        //   child: Center(
                        //       child: Icon(

                        //     color: Colors.black, //Colors.grey,
                        //   )),
                        // ),
                        SizedBox(
                          width: width * .02,
                        ),
                        Container(
                          width: width * .6,
                          child: TextFormField(
                            controller: passwordController,
                            decoration: InputDecoration(
                              hintText: '${getTranslated(context, "password")}',
                              hintStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "${getTranslated(context, "enter_password")}";
                              }
                              if (value.length < 8)
                                return "${getTranslated(context, "password_character")}";
                            },
                            obscureText: true,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height * .02,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: width * .15,
                          height: width * .15,
                          decoration: BoxDecoration(
                            color: Colors.grey[350],
                            borderRadius: BorderRadius.circular(
                              10,
                            ),
                          ),
                          child: Center(
                              child: Icon(
                            Icons.lock,
                            color: Colors.black,
                          )),
                        ),
                        SizedBox(
                          width: width * .02,
                        ),
                        Container(
                          width: width * .6,
                          child: TextFormField(
                            controller: confirmPassword,
                            decoration: InputDecoration(
                              hintText: '${getTranslated(context, "password")}',
                              hintStyle: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 18,
                              ),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "${getTranslated(context, "Confirm Password")}";
                              }
                              if (value.length < 8)
                                return "${getTranslated(context, "password_character")}";
                            },
                            obscureText: true,
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height * .07,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 50,
                          ),
                          child: Container(
                            width: width * .6,
                            height: height * .06,
                            child: FlatButton(
                              onPressed: () {
                                if (firstName.text.isEmpty ||
                                    lastName.text.isEmpty ||
                                    passwordController.text.isEmpty ||
                                    confirmPassword.text.isEmpty) {
                                  BotToast.showText(
                                    text: "please enter required field",
                                    duration: Duration(seconds: 2),
                                  );
                                } else if (passwordController.text !=
                                    confirmPassword.text) {
                                  BotToast.showText(
                                    text: "Password doesn't match",
                                    duration: Duration(seconds: 2),
                                  );
                                } else if (passwordController.text.length < 8) {
                                  BotToast.showText(
                                    text: "Password must be 8 characters",
                                    duration: Duration(seconds: 2),
                                  );
                                } else {
                                  BASEHELPER()
                                      .setProfile(
                                          firstName.text,
                                          lastName.text,
                                          mobileNumber,
                                          passwordController.text,
                                          context)
                                      .then((value) {
                                    if (value == null) {
                                      BotToast.showText(
                                        text: "something went wrong",
                                        duration: Duration(seconds: 2),
                                      );
                                    } else if (value['Status'] == false) {
                                      print("sign up response: $value");
                                      BotToast.showText(
                                        text: value["Message"]['EnResponse'],
                                        duration: Duration(seconds: 2),
                                      );
                                    } else {
                                      AppRoutes.replace(
                                          context,
                                          RegistrationScreen(
                                            driverId:
                                                value['DriverId'].toString(),
                                          ));
                                      BotToast.showText(
                                        text: value["Message"],
                                        duration: Duration(seconds: 2),
                                      );
                                    }
                                  }).catchError((error) {
                                    BotToast.showText(
                                      text: "$error",
                                      duration: Duration(seconds: 2),
                                    );
                                  });
                                }
                                // RestartWidget.restartApp(context);
                              },
                              splashColor: Colors.grey,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Container(
                                width: width,
                                height: height * .07,
                                child: Center(
                                  child: Text(
                                    // S.of(context).login,
                                    getTranslated(context, 'submit'),

                                    style: tripStatusTextStyle.copyWith(
                                      fontSize: 22,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              color: greyColorDark,
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.center,
                    //   children: [
                    //     GestureDetector(
                    //       onTap: () {
                    //         if (firstName.text.isEmpty ||
                    //             lastName.text.isEmpty ||
                    //             passwordController.text.isEmpty ||
                    //             confirmPassword.text.isEmpty) {
                    //           BotToast.showText(
                    //             text: "please enter required field",
                    //             duration: Duration(seconds: 2),
                    //           );
                    //         } else if (passwordController.text !=
                    //             confirmPassword.text) {
                    //           BotToast.showText(
                    //             text: "Password doesn't match",
                    //             duration: Duration(seconds: 2),
                    //           );
                    //         } else if (passwordController.text.length < 8) {
                    //           BotToast.showText(
                    //             text: "Password must be 8 characters",
                    //             duration: Duration(seconds: 2),
                    //           );
                    //         } else {
                    //           BASEHELPER()
                    //               .setProfile(
                    //                   firstName.text,
                    //                   lastName.text,
                    //                   mobileNumber,
                    //                   passwordController.text,
                    //                   context)
                    //               .then((value) {
                    //             if (value == null) {
                    //               BotToast.showText(
                    //                 text: "something went wrong",
                    //                 duration: Duration(seconds: 2),
                    //               );
                    //             } else if (value['Status'] == false) {
                    //               print("sign up response: $value");
                    //               BotToast.showText(
                    //                 text: value["Message"]['EnResponse'],
                    //                 duration: Duration(seconds: 2),
                    //               );
                    //             } else {
                    //               AppRoutes.replace(
                    //                   context,
                    //                   RegistrationScreen(
                    //                     driverId: value['DriverId'].toString(),
                    //                   ));
                    //               BotToast.showText(
                    //                 text: value["Message"],
                    //                 duration: Duration(seconds: 2),
                    //               );
                    //             }
                    //           }).catchError((error) {
                    //             BotToast.showText(
                    //               text: "$error",
                    //               duration: Duration(seconds: 2),
                    //             );
                    //           });
                    //         }
                    //       },
                    //       child: Card(
                    //         color: Colors.grey,
                    //         elevation: 5,
                    //         child: Container(
                    //           width: width * .5,
                    //           height: height * .06,
                    //           decoration: BoxDecoration(
                    //               borderRadius: BorderRadius.circular(10),
                    //               color: Colors.grey),
                    //           child: Center(
                    //             child: Text(
                    //               "${getTranslated(context, "submit")}",
                    //               style: TextStyle(
                    //                   color: Colors.white,
                    //                   fontSize: 18,
                    //                   fontWeight: FontWeight.bold),
                    //             ),
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
