import 'package:SathaCaptain/bloc/helper.dart';
import 'package:SathaCaptain/model/typesmodel.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/screens/login_screen.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/routes.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import '../app_router.dart';
import '../language_constants.dart';
import '../locator.dart';
import 'dart:io';
import 'dart:io' as Io;
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

class RegistrationScreen extends StatefulWidget {
  String driverId;
  RegistrationScreen({this.driverId});
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  var _navigationService = locator<NavigationService>();
  int seletedType;
  int id;
  File drivingLicence, qatarId;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false, selection = false;
  final TextEditingController firstNameController = new TextEditingController();
  final TextEditingController lastNameController = new TextEditingController();
  final TextEditingController mobileController = new TextEditingController();
  final TextEditingController vehicleNameController =
      new TextEditingController();
  final TextEditingController plateNumberController =
      new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  final TextEditingController userNameController = new TextEditingController();
  final TextEditingController addressController = new TextEditingController();
  final TextEditingController modalNo = new TextEditingController();
  var _userProvider;
  var qidfront, qidback, dlfront, dlback, licencefront, licenceback;
  String status = '';

  File tmpFile1;
  File tmpFile2;
  File tmpFile3;
  File tmpFile4;
  File tmpFile5;
  File tmpFile6;
  bool gallery = false;
  bool bottomSheet = false;
  String errMessage = 'Error Uploading Image';
  var width, height;

  Future<void> showRegisterSuccessfullAlertDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('Registered Successfuly!'),
          content: Text(
              'Waiting for Approval, You\'ll Get SMS Once Approved then You can Login'),
          actions: <Widget>[
//            CupertinoDialogAction(
//              child: Text('Cancel'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
            CupertinoDialogAction(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(builder: (context) => LoginScreen()),
                );
//                Navigator.pushReplacement(
//                  context,
//                  new MaterialPageRoute(builder: (context) => MainScreen()),
//                );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    //   var vehicleTypes = _userProvider.vehicleTypes(context);
    // }
  }

  @override
  Widget build(BuildContext context) {
    var dataProvider = Provider.of<DataProvider>(context);

    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    var _userProvider = Provider.of<DriverProvider>(context);
    return Scaffold(
      key: _scaffoldKey,
      bottomSheet: bottomSheet == true ? bottomsheet(id) : null,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          "${getTranslated(context, "documents")}",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        leading: InkWell(
          onTap: () {
            AppRoutes.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            size: 32,
            color: Colors.black,
          ),
        ),
      ),
      backgroundColor: greyColor, //greyColor,
      body: SafeArea(
        child: SingleChildScrollView(
          reverse: true,
          physics: BouncingScrollPhysics(),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: height * .01,
                ),
                // SizedBox(
                //     width: width * .5,
                //     child: Image.asset(
                //       "assets/images/satha_text.png",
                //     )),
                FutureBuilder<List<VehicleResult>>(
                  future: BASEHELPER().vehicleType(context),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data.length != 0) {
                        return Row(
                          children: [
                            Container(
                                width: width * .9,
                                height: height * .15,
                                margin: EdgeInsets.only(left: 10, right: 10),
                                color: Colors.transparent,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, int index) {
                                    return GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            seletedType =
                                                snapshot.data[index].id;
                                          });
                                        },
                                        child: Container(
                                            padding: EdgeInsets.all(10),
                                            decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            child: Center(
                                              child: Row(
                                                children: [
                                                  Container(
                                                    width: width * .08,
                                                    height: height * .06,
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: seletedType ==
                                                                snapshot
                                                                    .data[index]
                                                                    .id
                                                            ? mainColor
                                                            : Colors.grey
                                                                .withOpacity(
                                                                    0.3)),
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Text(
                                                    "${snapshot.data[index].name}",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                  )
                                                ],
                                              ),
                                            )));
                                  },
                                  itemCount: snapshot.data.length,
                                )),
                          ],
                        );
                      } else {
                        return Center(
                          child: Text(
                            "no vehicle type found",
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        );
                      }
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  },
                ),
                SizedBox(
                  height: height * .01,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: width * .15,
                    ),
                    Text(
                      "${getTranslated(context, "vehicle_detail")}",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                SizedBox(
                  height: height * .02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Container(
                    //   width: width * .10,
                    //   height: width * .10,
                    //   decoration: BoxDecoration(
                    //     color: Colors.grey, //Colors.grey[350],
                    //     borderRadius: BorderRadius.circular(
                    //       10,
                    //     ),
                    //   ),
                    //   child: Center(
                    //       child: Icon(Icons.car_rental,
                    //           color: Colors.black //Colors.grey,
                    //           )),
                    // ),
                    Container(
                      width: width * .15,
                      height: width * .15,
                      decoration: BoxDecoration(
                        color: Colors.grey[350],
                        borderRadius: BorderRadius.circular(
                          10,
                        ),
                      ),
                      child: Center(
                          child: Icon(
                        Icons.car_rental,
                        color: Colors.black,
                      )),
                    ),
                    SizedBox(
                      width: width * .02,
                    ),
                    Container(
                      width: width * .6,
                      child: TextFormField(
                        controller: vehicleNameController,
                        decoration: InputDecoration(
                          hintText: '${getTranslated(context, "vehicle_name")}',
                          hintStyle: tripStatusTextStyle.copyWith(
                            color: Theme.of(context).primaryColor,
                            fontSize: 18,
                          ),
                        ),
                        // onSaved: (newValue) {
                        //   mobileNumber = newValue;
                        // },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "${getTranslated(context, "enter_vehicle_name")}";
                          }
                          // if (value.length < 8) {
                          //   return 'Please enter full number';
                          // }
                        },
                        //maxLength: 8,
                        keyboardType: TextInputType.streetAddress,
                        style: tripStatusTextStyle.copyWith(
                          color: Colors.black87,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Container(
                    //   width: width * .10,
                    //   height: width * .10,
                    //   decoration: BoxDecoration(
                    //     color: Colors.grey, //Colors.grey[350],
                    //     borderRadius: BorderRadius.circular(
                    //       10,
                    //     ),
                    //   ),
                    //   child: Center(
                    //       child: Icon(Icons.car_rental,
                    //           color: Colors.black //Colors.grey,
                    //           )),
                    // ),
                    Container(
                      width: width * .15,
                      height: width * .15,
                      decoration: BoxDecoration(
                        color: Colors.grey[350],
                        borderRadius: BorderRadius.circular(
                          10,
                        ),
                      ),
                      child: Center(
                          child: Icon(
                        Icons.car_rental,
                        color: Colors.black,
                      )),
                    ),
                    SizedBox(
                      width: width * .02,
                    ),
                    Container(
                      width: width * .6,
                      child: TextFormField(
                        controller: modalNo,
                        decoration: InputDecoration(
                          hintText: '${getTranslated(context, "model_no")}',
                          hintStyle: tripStatusTextStyle.copyWith(
                            color: Theme.of(context).primaryColor,
                            fontSize: 18,
                          ),
                        ),
                        // onSaved: (newValue) {
                        //   mobileNumber = newValue;
                        // },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "${getTranslated(context, "model_no")}";
                          }
                          // if (value.length < 8) {
                          //   return 'Please enter full number';
                          // }
                        },
                        //maxLength: 8,
                        keyboardType: TextInputType.streetAddress,
                        style: tripStatusTextStyle.copyWith(
                          color: Colors.black87,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .01,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: width * .15,
                      height: width * .15,
                      decoration: BoxDecoration(
                        color: Colors.grey[350],
                        borderRadius: BorderRadius.circular(
                          10,
                        ),
                      ),
                      child: Center(
                          child: Icon(
                        Icons.confirmation_num,
                        color: Colors.black,
                      )),
                    ),
                    // Container(
                    //   width: width * .10,
                    //   height: width * .10,
                    //   decoration: BoxDecoration(
                    //     color: Colors.grey, //Colors.grey[350],
                    //     borderRadius: BorderRadius.circular(
                    //       10,
                    //     ),
                    //   ),
                    //   child: Center(
                    //       child: Icon(Icons.confirmation_num,
                    //           color: Colors.black //Colors.grey,
                    //           )),
                    // ),
                    SizedBox(
                      width: width * .02,
                    ),
                    Container(
                      width: width * .6,
                      child: TextFormField(
                        maxLength: 6,
                        controller: plateNumberController,
                        decoration: InputDecoration(
                          hintText: '${getTranslated(context, "plate_no")}',
                          hintStyle: tripStatusTextStyle.copyWith(
                            color: Theme.of(context).primaryColor,
                            fontSize: 18,
                          ),
                        ),
                        // onSaved: (newValue) {
                        //   mobileNumber = newValue;
                        // },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "${getTranslated(context, "enter_plate_no")}";
                          }
                          // if (value.length < 8) {
                          //   return 'Please enter full number';
                          // }
                        },
                        //maxLength: 8,
                        keyboardType: TextInputType.phone,
                        style: tripStatusTextStyle.copyWith(
                          color: Colors.black87,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Qid front",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              id = 1;
                              bottomSheet = true;
                            });
                          },
                          child: Container(
                            width: width * .25,
                            height: height * .08,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: tmpFile1 != null
                                ? Image.file(
                                    tmpFile1,
                                    fit: BoxFit.contain,
                                    width: width * .25,
                                    height: height * .08,
                                  )
                                : Center(
                                    child: Icon(Icons.add),
                                  ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          "Qid Back",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              id = 2;
                              bottomSheet = true;
                            });
                          },
                          child: Container(
                            width: width * .25,
                            height: height * .08,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: tmpFile2 != null
                                ? Image.file(
                                    tmpFile2,
                                    fit: BoxFit.contain,
                                    width: width * .25,
                                    height: height * .08,
                                  )
                                : Center(
                                    child: Icon(Icons.add),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .01,
                ),
                SizedBox(
                  height: height * .02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        Text(
                          "DL front",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              id = 3;
                              bottomSheet = true;
                            });
                          },
                          child: Container(
                            width: width * .25,
                            height: height * .08,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: tmpFile3 != null
                                ? Image.file(
                                    tmpFile3,
                                    fit: BoxFit.contain,
                                    width: width * .25,
                                    height: height * .08,
                                  )
                                : Center(
                                    child: Icon(Icons.add),
                                  ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          "DL Back",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              id = 4;
                              bottomSheet = true;
                            });
                          },
                          child: Container(
                            width: width * .25,
                            height: height * .08,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: tmpFile4 != null
                                ? Image.file(
                                    tmpFile4,
                                    fit: BoxFit.contain,
                                    width: width * .25,
                                    height: height * .08,
                                  )
                                : Center(
                                    child: Icon(Icons.add),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Istimara front",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              id = 5;
                              bottomSheet = true;
                            });
                          },
                          child: Container(
                            width: width * .25,
                            height: height * .08,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: tmpFile5 != null
                                ? Image.file(
                                    tmpFile5,
                                    fit: BoxFit.contain,
                                    width: width * .25,
                                    height: height * .08,
                                  )
                                : Center(
                                    child: Icon(Icons.add),
                                  ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          "Istimara Back",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              id = 6;
                              bottomSheet = true;
                            });
                          },
                          child: Container(
                            width: width * .25,
                            height: height * .08,
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: tmpFile6 != null
                                ? Image.file(
                                    tmpFile6,
                                    fit: BoxFit.contain,
                                    width: width * .25,
                                    height: height * .08,
                                  )
                                : Center(
                                    child: Icon(Icons.add),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .01,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 50,
                  ),
                  child: FlatButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        // signUp();
                        if (seletedType == null) {
                          BotToast.showText(
                            text: "Please select Vehicle Type",
                            duration: Duration(seconds: 2),
                          );
                        } else if (tmpFile1 == null ||
                            tmpFile2 == null ||
                            tmpFile3 == null ||
                            tmpFile4 == null ||
                            tmpFile5 == null ||
                            tmpFile6 == null) {
                          BotToast.showText(
                            text:
                                "Please select license, istimara & qatar id images",
                            duration: Duration(seconds: 2),
                          );
                        } else {
                          print("hello");
                          BASEHELPER()
                              .signUp(
                                  vehicleNameController.text,
                                  plateNumberController.text,
                                  seletedType,
                                  modalNo.text,
                                  "${qidfront.toString()}",
                                  "${qidback.toString()}",
                                  "${dlfront.toString()}",
                                  "${dlback.toString()}",
                                  "${licencefront.toString()}",
                                  "${licenceback.toString()}",
                                  widget.driverId.toString(),
                                  context)
                              .then((value) {
                            if (value == null) {
                              BotToast.showText(
                                text: "something went wrong",
                                duration: Duration(seconds: 2),
                              );
                            } else if (value['Status'] == true) {
                              print("sign up response: $value");
                              BotToast.showText(
                                text: value["Message"],
                                duration: Duration(seconds: 2),
                              );
                              AppRoutes.makeFirst(context, LoginScreen());
                            } else {
                              BotToast.showText(
                                text: value["Message"],
                                duration: Duration(seconds: 2),
                              );
                            }
                          }).catchError((error) {
                            BotToast.showText(
                              text: "$error",
                              duration: Duration(seconds: 2),
                            );
                          });
                        }
                      }
                    },
                    splashColor: Colors.grey,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: width,
                      height: height * .055,
                      child: Center(
                        child: _isLoading
                            ? CircularProgressIndicator()
                            : Text(
                                '${getTranslated(context, "Sign Up")}',
                                style: tripStatusTextStyle.copyWith(
                                  fontSize: 20,
                                  color: Theme.of(context).bottomAppBarColor,
                                ),
                              ),
                      ),
                    ),
                    color: Theme.of(context).primaryColor, //greyColorDark,
                  ),
                ),
                SizedBox(
                  height: height * .02,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '${getTranslated(context, "already_account")}',
                      style: tripStatusTextStyle.copyWith(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: InkWell(
                        onTap: () => _navigationService.navigateTo(
                          AppRoute.loginScreen,
                        ),
                        child: Text(
                          '${getTranslated(context, "login")}',
                          style: tripStatusTextStyle.copyWith(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * .02,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget bottomsheet(int id) {
    return Container(
        height: MediaQuery.of(context).size.height * .3,
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * .02),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
            top: 10,
            right: 10,
            left: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          color: Colors.white,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: height * .03,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      bottomSheet = false;
                    });
                  },
                  child: Icon(Icons.close, size: height * .04),
                )
              ],
            ),
            Container(
                width: MediaQuery.of(context).size.width * .95,
                height: MediaQuery.of(context).size.height * .2,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .03,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            BASEHELPER().chooseImage(true).then((value) {
                              print("selectedc images: $value");
                              final bytes =
                                  Io.File(value.path).readAsBytesSync();
                              final img = base64Encode(bytes);
                              print(img);
                              if (id == 1) {
                                setState(() {
                                  tmpFile1 = value;
                                  qidfront = img;
                                  bottomSheet = false;
                                });
                                print(tmpFile1);
                              } else if (id == 2) {
                                setState(() {
                                  tmpFile2 = value;
                                  qidback = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 3) {
                                setState(() {
                                  tmpFile3 = value;
                                  dlfront = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 4) {
                                setState(() {
                                  tmpFile4 = value;
                                  dlback = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 5) {
                                setState(() {
                                  tmpFile5 = value;
                                  licencefront = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 6) {
                                setState(() {
                                  tmpFile6 = value;
                                  licenceback = img;
                                  bottomSheet = false;
                                });
                              }
                            });
                          },
                          child: Container(
                            width: width * .5,
                            height: height * .06,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: mainColor),
                            child: Center(
                              child: Text(
                                "Gallery",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .03,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            BASEHELPER().chooseImage(false).then((value) {
                              print("selectedc images: $value");
                              // final bytes = File(value).readAsBytesSync();
                              final img = base64Encode(value.readAsBytesSync())
                                  .toString();
                              print(img);
                              if (id == 1) {
                                setState(() {
                                  tmpFile1 = value;
                                  qidfront = img;
                                  bottomSheet = false;
                                });
                                print(tmpFile1);
                              } else if (id == 2) {
                                setState(() {
                                  tmpFile2 = value;
                                  qidback = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 3) {
                                setState(() {
                                  tmpFile3 = value;
                                  dlfront = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 4) {
                                setState(() {
                                  tmpFile4 = value;
                                  dlback = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 5) {
                                setState(() {
                                  tmpFile5 = value;
                                  licencefront = img;
                                  bottomSheet = false;
                                });
                              } else if (id == 6) {
                                setState(() {
                                  tmpFile6 = value;
                                  licenceback = img;
                                  bottomSheet = false;
                                });
                              }
                            });
                          },
                          child: Container(
                            width: width * .5,
                            height: height * .06,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.grey),
                            child: Center(
                              child: Text(
                                "Camera",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ))
          ],
        ));
  }
}
