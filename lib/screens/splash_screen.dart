import 'dart:io';

import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/globals.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String message;

  @override
  void initState() {
    super.initState();

    var _dataProvider = Provider.of<DataProvider>(context, listen: false);

    Provider.of<MapBoxLocationProvider>(context, listen: false)
        .getUserLocation();
    Provider.of<DriverProvider>(context, listen: false)
        .initialiseProgressDialog(context);
    // _dataProvider.setPushNotificationEnabled(true, context);
    // PushNotificationService pushNotificationService =
    //     PushNotificationService();
    // pushNotificationService.init(context: context);
    if (_dataProvider.isInternetAvailable) {
      initializeFirebase();
      Future.delayed(Duration(seconds: 3), () {
        Provider.of<DriverProvider>(context, listen: false).checkLogin(context);
      });
    } else {
      Future.delayed(
          Duration(
            seconds: 6,
          ), () {
        Provider.of<DataProvider>(context, listen: false)
            .checkSelectedLanguage();

        Provider.of<DriverProvider>(context, listen: false).checkLogin(context);
      });
    }
  }

  bool _initialized = false;
  bool _error = false;

  void initializeFirebase() async {
    try {
      final result = await InternetAddress.lookup('www.example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        print(result);
        try {
          await Firebase.initializeApp();
          setState(() => _initialized = true);
        } catch (e) {
          setState(() => _error = true);
          print(e.toString());
        }
      }
    } on SocketException catch (_) {
      print('not connected');
      setState(() => _error = true);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              splash_screen,
            ),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
