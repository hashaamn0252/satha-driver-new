import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../language_constants.dart';

class PreviousHistory extends StatefulWidget {
  PreviousHistory({Key key}) : super(key: key);

  @override
  _PreviousHistoryState createState() => _PreviousHistoryState();
}

class _PreviousHistoryState extends State<PreviousHistory> {
  TextEditingController _startingDateController =
      TextEditingController(text: DateTime.now().toString());

  TextEditingController _endDateController =
      TextEditingController(text: DateTime.now().toString());

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _dataProvider = Provider.of<DataProvider>(context);
    var _driverProvider = Provider.of<DriverProvider>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 26,
          ),
        ),
        automaticallyImplyLeading: false,
        title: Text(
          // S.of(context).previousHistory,
          getTranslated(context, 'previousHistory'),

          style: nameTextStyle.copyWith(
            color: Colors.black,
            fontSize: 22,
          ),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 12,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  // S.of(context).startDate,
                  getTranslated(context, 'startDate'),

                  style: tripStatusTextStyle.copyWith(
                    color: greyColorDark,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  width: width * .4,
                  child: DateTimePicker(
                    type: DateTimePickerType.date,
                    initialValue: _dataProvider.startDate,
                    firstDate: DateTime(
                      2000,
                    ),
                    lastDate: DateTime(3000),
                    dateHintText: "Date",
                    cursorColor: mainColor,
                    expands: false,
                    onChanged: (value) {
                      _dataProvider.changeStartDate(value);
                    },
                    onSaved: (newValue) {
                      _dataProvider.changeStartDate(newValue);
                    },
                    use24HourFormat: false,
                    decoration: InputDecoration(
                      hintStyle:
                          tripStatusTextStyle.copyWith(color: greyColorDark),
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(0),
                      suffixIconConstraints: BoxConstraints.tightFor(),
                      suffixIcon: Icon(
                        Icons.edit,
                        size: 22,
                        color: Colors.black,
                      ),
                      alignLabelWithHint: true,
                    ),
                    style: tripStatusTextStyle.copyWith(color: greyColorDark),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 12,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  // S.of(context).endDate,
                  getTranslated(context, 'endDate'),

                  style: tripStatusTextStyle.copyWith(
                    color: greyColorDark,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  width: width * .4,
                  child: DateTimePicker(
                    type: DateTimePickerType.date,
                    initialValue: _dataProvider.endDate,
                    firstDate: DateTime(
                      2000,
                    ),
                    lastDate: DateTime(3000),
                    dateHintText: "Date",
                    cursorColor: mainColor,
                    onSaved: (v) => _dataProvider.changeEndDate(v),
                    expands: false,
                    decoration: InputDecoration(
                      hintStyle:
                          tripStatusTextStyle.copyWith(color: greyColorDark),
                      border: InputBorder.none,
                      suffixIconConstraints: BoxConstraints.tightFor(),
                      contentPadding: EdgeInsets.all(0),
                      suffixIcon: Icon(
                        Icons.edit,
                        size: 22,
                        color: Colors.black,
                      ),
                      alignLabelWithHint: true,
                    ),
                    style: tripStatusTextStyle.copyWith(color: greyColorDark),
                    onChanged: (date) {
                      _dataProvider.changeEndDate(date);
                    },
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: FlatButton(
              color: greyColorDark,
              onPressed: () async {
                _driverProvider.initialiseProgressDialog(context);

                print(_dataProvider.startDate);
                print(_dataProvider.endDate);
                _driverProvider.progressDialog.show();

                await _driverProvider.driverHistory(
                  _dataProvider.startDate,
                  _dataProvider.endDate,
                  context,
                );
                _driverProvider.progressDialog.hide();
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  10,
                ),
              ),
              padding: EdgeInsets.all(0),
              splashColor: Colors.grey,
              child: Container(
                width: width,
                height: height * .07,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      // S.of(context).search.toUpperCase(),
                      getTranslated(context, 'search').toUpperCase(),

                      style: tripStatusTextStyle.copyWith(
                        color: Colors.white,
                        fontSize: 22,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: height * .04,
          ),
          Row(
            children: [
              SizedBox(
                width: width * .05,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Hello,',
                    style: tripStatusTextStyle.copyWith(
                      fontSize: 22,
                      color: greyColorDark,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  Text(
                    '${_driverProvider.driver.firstName}' +
                        " " +
                        "${_driverProvider.driver.lastName}",
                    style: tripStatusTextStyle.copyWith(
                      fontSize: 22,
                      color: greyColorDark,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: height * .01,
          ),
          _driverProvider.isButtonPressed
              ? _driverProvider.tripHistroyWidget
              : SizedBox(),
        ],
      ),
    );
  }
}
