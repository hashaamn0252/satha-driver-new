import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:SathaCaptain/bloc/basehelper.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/model/driver.dart';
import 'package:SathaCaptain/model/singleton.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/constants.dart';
import 'package:SathaCaptain/utilis/globals.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:SathaCaptain/widgets/custom_drawer.dart';
import 'package:SathaCaptain/widgets/floating_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:SathaCaptain/utilis/globals.dart' as globals;
import 'package:http/http.dart' as http;

import '../language_constants.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
// var _navigationService = locator<NavigationService>();
  Color color = Colors.red;
  String status = '';
  String tripStatus = '';
  Widget cupertino = SizedBox();
  var _navigationService = locator<NavigationService>();
  final Completer<GoogleMapController> _mapController = Completer();
  Location location = Location();
  LocationData currentLocation;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  String lat, long;
  String driverId;

  bool isLoading;
  Set<Marker> _markers = Set<Marker>();

  changeLocation(BuildContext context) async {
    bool err = false;
    var _locationProvider = Provider.of<MapBoxLocationProvider>(
      context,
      listen: false,
    );
    Future.delayed(
        Duration(
          seconds: 1,
        ), () {
      setState(() {
        isLoading = false;
        _markers.add(
          Marker(
            markerId: MarkerId('pickUpPosition'),
            position: LatLng(
              _locationProvider.currentLocation.latitude,
              _locationProvider.currentLocation.longitude,
            ),
            icon: BitmapDescriptor.defaultMarker,
          ),
        );
      });
    });

    location.onLocationChanged.listen((event) async {
      String checkDriverId = await getUserIdPrefs();

      String driverIdd = Provider.of<DriverProvider>(context, listen: false)
          .driver
          .id
          .toString();

      _locationProvider.changeLatLng(
          currentLocation.latitude, currentLocation.longitude);

      currentLocation = event;
      lat = event.latitude.toString();
      long = event.longitude.toString();
      var requestBody = {
        "DriverId": driverIdd,
        "Latitude": lat,
        "Longitude": long,
      };
      print(checkDriverId);
      print(driverIdd);
      // cameraMove(lat, long);

      await http
          .post(
        updateCurrentLatLngURL,
        body: requestBody,
        headers: webAPIKey,
      )
          .catchError(
        (error) {
          err = true;
          print("Exception in API calling =====>>>> $error");
        },
      ).then((value) {
        var response = json.decode(value.body);

        if (!err) {
          print(
              "Simutaneously Location is Updating after one minute ====>>> $response");
        }
      });
    });
  }

  cameraMove(
    String driverLat,
    String driverLong,
  ) async {
    double lat = double.parse(driverLat);
    double long = double.parse(driverLong);
    var currentPosition = LatLng(
      currentLocation.latitude,
      currentLocation.longitude,
    );

    CameraPosition newCameraPosition = CameraPosition(
      target: LatLng(lat, long),
      zoom: 16,
    );
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        newCameraPosition,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    PushNotificationService();
    changeLocation(context);
    PushNotificationService pushNotificationService = PushNotificationService();
    pushNotificationService.init(context: context);
    isLoading = true;
    setState(() {});
  }

  @override
  void dispose() {
    // globals.driverStatusChangeWhenAppKill();
    // print("heasd");
    // APIHELPER().offlineUser(context);
    // if (Userss.userData.driverId != null) {
    //   print("driver id: ${Userss.userData.driverId}");
    //   var data = {
    //     "DriverId": Userss.userData.driverId,
    //     "Status": "Not Available"
    //   };
    //   print(data);
    //   Future.delayed(Duration(milliseconds: 500), () async {
    //     await http.post(updateStatusURL, headers: webAPIKey, body: data);
    //   });
    // } else {
    //   // print("driver id: ${Userss.userData.driverId}");
    // }
    // Provider.of<DriverProvider>(context, listen: false)
    //     .updateCurrentStatus("Not Available");

    // if (Userss.userData.driverId != null) {
    //   print("driver id: ${Userss.userData.driverId}");
    //   var data = {
    //     "DriverId": Userss.userData.driverId,
    //     "Status": "Not Available"
    //   };
    //   await http.post(updateStatusURL, headers: webAPIKey, body: data).then(
    //     (value) {
    //       print("heasd");
    //       var response = json.decode(value.body);
    //       //STATUS
    //       print(response);
    //     },
    //   );
    // }
    // Provider.of<DriverProvider>(context, listen: false)
    //     .updateCurrentStatus("Not Available");
    print("heasd");
    super.dispose();
  }

  @override
  Future<void> deactivate() async {
    // var _dataProvider = Provider.of<DataProvider>(context, listen: false);
    if (Userss.userData.driverId != null) {
      print("driver id: ${Userss.userData.driverId}");

      // _dataProvider.setPushNotificationEnabled(false, context);
      // callApi();
      // globals.driverStatusChangeWhenAppKill();
      // super.deactivate();
    } else {
      // globals.driverStatusChangeWhenAppKill();

      // print("driver id: ${Userss.userData.driverId}");
    }
  }

  StreamController<Driver> _controller;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    // var _dataProvider = Provider.of<DataProvider>(context);
    var _mapboxLocationProvider = Provider.of<MapBoxLocationProvider>(context);
    var _userProvider = Provider.of<DriverProvider>(context);

    return WillPopScope(
      // ignore: missing_return
      onWillPop: () {
        // Provider.of<DriverProvider>(context, listen: false)
        //     .updateCurrentStatus("Not Available");
        // globals.driverStatusChangeWhenAppKill();
        if (Platform.isIOS) {
          try {
            exit(0);
          } catch (e) {
            SystemNavigator
                .pop(); // for IOS, not true this, you can make comment this :)
          }
        } else {
          try {
            SystemNavigator.pop(); // sometimes it cant exit app
          } catch (e) {
            exit(0); // so i am giving crash to app ... sad :(
          }
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Container(
                  height: height * .954,
                  width: width,
                  child: Stack(
                    // fit: StackFit.loose,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    children: [
                      isLoading
                          ? CupertinoActivityIndicator()
                          : GoogleMap(
                              onTap: (argument) async {
                                _userProvider.logout();
                              },
                              initialCameraPosition: CameraPosition(
                                target: LatLng(
                                  _mapboxLocationProvider.currentLocation !=
                                          null
                                      ? _mapboxLocationProvider
                                          .currentLocation.latitude
                                      : 0.0,
                                  _mapboxLocationProvider.currentLocation !=
                                          null
                                      ? _mapboxLocationProvider
                                          .currentLocation.longitude
                                      : 0.0,
                                ),
                                zoom: 15.0,
                              ),
                              mapType: MapType.normal,
                              tiltGesturesEnabled: false,
                              myLocationButtonEnabled: true,
                              myLocationEnabled: true,

                              padding: EdgeInsets.only(
                                top: height * .7,
                              ),
                              // markers: _markers,
                              onMapCreated: (GoogleMapController controller) {
                                _mapController.complete(controller);

                                controller.animateCamera(
                                  CameraUpdate.newCameraPosition(
                                    CameraPosition(
                                      target: LatLng(
                                        _mapboxLocationProvider
                                            .currentLocation.latitude,
                                        _mapboxLocationProvider
                                            .currentLocation.longitude,
                                        // currentLocation.latitude,
                                        // currentLocation.longitude,
                                      ),
                                      zoom: 15.0,
                                    ),
                                  ),
                                );
                              },
                              // onCameraMove: ,
                              // ),
                            ),
                      Positioned(
                        width: width,
                        top: height * -.01,
                        height: height * .18,
                        child: dialogs(),
                        // child: FloatingDialog(
                        //   openEndDrawer: () {
                        //     _scaffoldKey.currentState.openEndDrawer();
                        //   },
                        // ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        endDrawer: CustomDrawer(endDrawer: () {
          Navigator.pop(context);
        }),
      ),
    );
  }

  Widget dialogs() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var _locationProvider = Provider.of<MapBoxLocationProvider>(context);
    var _dataProvider = Provider.of<DataProvider>(context);
    var _userProvider = Provider.of<DriverProvider>(context);
    print(_userProvider.currentTripStatus);
    if (_userProvider.currentTripStatus == null) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');

        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              // status = S.of(context).online;
              status = getTranslated(context, 'online');

              color = Colors.green;
              tripStatus = getTranslated(context, 'waitingForRide');
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              // status = S.of(context).offline;
              status = getTranslated(context, 'offline');
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'No Trip'.toLowerCase()) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');
        color = Colors.green;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("AVAILABLE");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.isEmpty) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        // status = S.of(context).online;
        status = getTranslated(context, 'online');
        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'On Ride'.toLowerCase()) {
      // status = S.of(context).busy;
      status = getTranslated(context, 'busy');
      tripStatus = '';
      _dataProvider.changeWidget(SizedBox());
      color = Colors.red;
    } else if (_userProvider.currentTripStatus.toLowerCase() ==
        'Available'.toLowerCase()) {
      if (!_locationProvider.goLive) {
        tripStatus = '';
        color = Colors.red;
        // status = S.of(context).offline;
        status = getTranslated(context, 'offline');
      } else {
        status = 'Online';
        color = Colors.green;
        // tripStatus = S.of(context).waitingForRide;
        tripStatus = getTranslated(context, 'waitingForRide');
      }
      _dataProvider.changeWidget(CupertinoSwitch(
        trackColor: Colors.red,
        value:
            Provider.of<MapBoxLocationProvider>(context, listen: false).goLive,
        onChanged: (val) async {
          _dataProvider.setPushNotificationEnabled(true, context);
          // PushNotificationService pushNotificationService =
          //     PushNotificationService();
          // pushNotificationService.init(context: context);
          _userProvider.logout();

          if (val) {
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Available");
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Online';
              color = Colors.green;
              tripStatus = 'Waiting for ride...';
            });
          } else {
            _dataProvider.setPushNotificationEnabled(false, context);
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentLatLng(
                    _locationProvider.currentLocation.latitude.toString(),
                    _locationProvider.currentLocation.longitude.toString());
            Provider.of<DriverProvider>(context, listen: false)
                .updateCurrentStatus("Not Available");
            setState(() {
              Provider.of<MapBoxLocationProvider>(context, listen: false)
                  .changeGoLive(val, context);
              status = 'Offline';
              color = Colors.red;
              tripStatus = '';
            });
          }
        },
      ));
    } else {
      // status = S.of(context).busy;
      status = getTranslated(context, 'busy');
      tripStatus = '';
      _dataProvider.changeWidget(SizedBox());
      color = Colors.red;
    }

    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.transparent,
        image: DecorationImage(
          image: AssetImage(
            floating_dialog_background,
          ),
          fit: BoxFit.fill,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 12,
        ),
        child: Row(
          children: [
            SizedBox(
                width: width * .2,
                height: width * .2,
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage(
                    profile_avataar,
                  ),
                )),
            SizedBox(
              width: width * .02,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      // S.of(context).driver,
                      getTranslated(context, 'driver'),
                      style: tripStatusTextStyle.copyWith(
                        fontSize: 22,
                        color: greyColorDark,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      width: width * .15,
                    ),
                    _dataProvider.cupertino,
                  ],
                ),
                Text(
                  status,
                  style: tripStatusTextStyle.copyWith(
                    fontSize: 14,
                    color: color,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  tripStatus,
                  style: tripStatusTextStyle,
                ),
                SizedBox(
                  height: height * .02,
                ),
              ],
            ),
            Spacer(),
            SizedBox(
              width: width * .2,
              height: width * .2,
              child: ClipOval(
                child: Material(
                  child: InkWell(
                    onTap: () async {
                      _dataProvider.showEndDrawerEnabled(true);
                      _scaffoldKey.currentState.openEndDrawer();
                      // PushNotificationService pushNotificationService =
                      //     PushNotificationService();
                      // _userProvider.logout();
                    },
                    splashColor: Colors.black,
                    child: SizedBox(
                      child: Image.asset(
                        menu_icon2,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
