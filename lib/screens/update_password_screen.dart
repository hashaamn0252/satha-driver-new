import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../language_constants.dart';
import '../locator.dart';

class UpdatePasswordScreen extends StatefulWidget {
  UpdatePasswordScreen({Key key}) : super(key: key);

  @override
  _UpdatePasswordScreenState createState() => _UpdatePasswordScreenState();
}

class _UpdatePasswordScreenState extends State<UpdatePasswordScreen> {
  @override
  void dispose() {
    Provider.of<DriverProvider>(context, listen: false)
        .confirmPasswordTextEditingController
        .dispose();

    Provider.of<DriverProvider>(context, listen: false)
        .newPasswordTextEditingController
        .dispose();
    super.dispose();
  }

  // var _navigationService = locator<NavigationService>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _userProvider = Provider.of<DriverProvider>(context);
    return Scaffold(
      backgroundColor: greyColor,
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.arrow_back,
            size: 32,
            color: Colors.black,
          ),
        ),
      ),
      extendBody: true,
      body: SafeArea(
        child: SingleChildScrollView(
          reverse: true,
          physics: BouncingScrollPhysics(),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                SizedBox(
                  height: height * .05,
                ),
                SizedBox(
                    width: width * .7,
                    child: Image.asset(
                      sathaLogo,
                    )),
                SizedBox(
                  height: height * .02,
                ),
                Text(
                  // S.of(context).changePassword,
                  getTranslated(context, 'changePassword'),

                  style: nameTextStyle.copyWith(
                    color: Colors.black87,
                  ),
                ),
                SizedBox(
                  height: height * .01,
                ),
                Text(
                  // S.of(context).newPasswordText,
                  getTranslated(context, 'newPasswordText'),
                ),
                SizedBox(
                  height: height * .1,
                ),
                Container(
                  width: width * .8,
                  child: TextFormField(
                    obscureText: true,
                    controller: _userProvider.newPasswordTextEditingController,
                    decoration: InputDecoration(
                      // hintText: S.of(context).newPassword,
                      hintText: getTranslated(context, 'newPassword'),

                      hintStyle: tripStatusTextStyle.copyWith(
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                    style: tripStatusTextStyle.copyWith(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        // return S.of(context).fieldEmptyText;
                        return getTranslated(context, 'fieldEmptyText');
                      }
                      if (value.length < 8) {
                        // return S.of(context).passwordLength;
                        return getTranslated(context, 'passwordLength');
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: height * .03,
                ),
                Container(
                  width: width * .8,
                  child: TextFormField(
                    obscureText: true,
                    controller:
                        _userProvider.confirmPasswordTextEditingController,
                    decoration: InputDecoration(
                      hintText:
                          // S.of(context).confirm + S.of(context).password,
                          getTranslated(context, 'confirm') +
                              getTranslated(context, 'password'),
                      hintStyle: tripStatusTextStyle.copyWith(
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                    style: tripStatusTextStyle.copyWith(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        // return S.of(context).fieldEmptyText;
                        return getTranslated(context, 'fieldEmptyText');
                      }
                      if (value !=
                          _userProvider.newPasswordTextEditingController.text) {
                        // return S.of(context).passwordMisMatched;
                        return getTranslated(context, 'passwordMisMatched');
                      }
                      if (value.length < 8) {
                        // return S.of(context).passwordLength;
                        return getTranslated(context, 'passwordLength');
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: height * .03,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 40,
                  ),
                  child: FlatButton(
                    splashColor: Colors.grey,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _userProvider.updatePassword(
                            _userProvider.newPasswordTextEditingController.text
                                .trim(),
                            context);
                        _userProvider.clear();
                      }
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: width,
                      height: height * .07,
                      child: Center(
                        child: Text(
                          // S.of(context).submit.toUpperCase(),
                          getTranslated(context, 'submit').toUpperCase(),

                          style: tripStatusTextStyle.copyWith(
                            fontSize: 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    color: greyColorDark,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
