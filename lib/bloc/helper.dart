import 'dart:convert';
import 'dart:io';

import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/model/driver.dart';
import 'package:SathaCaptain/model/trip_details.dart';
import 'package:SathaCaptain/model/typesmodel.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/constants.dart';
import 'package:SathaCaptain/utilis/device_info.dart';
import 'package:SathaCaptain/utilis/globals.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:SathaCaptain/widgets/trip_history.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';

class BASEHELPER {
  ProgressDialog progressDialog;
  initialiseProgressDialog(BuildContext context) {
    progressDialog = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );
    progressDialog.style(
        message: 'Please wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: tripStatusTextStyle.copyWith(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: tripStatusTextStyle.copyWith(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
  }

  Future<List<VehicleResult>> vehicleType(context) async {
    print(vehicleTypes);
    VehicleTypesModel vehicleTypesModel = VehicleTypesModel();
    String language = await getLanguage();
    try {
      var res = await http.get(vehicleTypes, headers: webAPIKey);
      var response = json.decode(res.body);
      print("vehicle types: $response");
      if (res.statusCode == 200) {
        if (response['Status'] == true) {
          vehicleTypesModel = VehicleTypesModel.fromJson(response);
          return vehicleTypesModel.result;
        } else {
          BotToast.showText(
            text: response["Message"],
            duration: Duration(seconds: 2),
          );
          return vehicleTypesModel.result;
        }
      } else {
        BotToast.showText(
          text: response["Message"],
          duration: Duration(seconds: 2),
        );
      }
    } on SocketException {
      BotToast.showText(
        text: "No Internet Connection",
        duration: Duration(seconds: 2),
      );
    } on HttpException catch (error) {
      print(error);
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    } on FormatException catch (error) {
      print(error);

      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
      // EasyLoading.dismiss();
      print("Bad response format 👎");
    } catch (error) {
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    }
  }

  Future signUp(vehicleName, plateNo, typeId, modelNo, qid, qidBack, dl, dlBack,
      istimara, istimaraBack, driverId, BuildContext context) async {
    print(driverRegistrationNext);
    initialiseProgressDialog(context);
    progressDialog.show();
    String language = await getLanguage();
    var data = {
      "DriverId": "$driverId",
      "QatarId": "$qid",
      "QatarIdBack": "$qidBack",
      "DrivingLicense": "$dl",
      "DrivingLicenseBack": "$dlBack",
      "Istimara": "$istimara",
      "IstimaraBack": "$istimaraBack",
      "VehicleName": "$vehicleName",
      "ModalNo": "$modelNo",
      "PlateNo": "$plateNo",
      "VehicleType": "$typeId",
    };
    var header = {
      'Access-Control-Allow-Origin': "*",
      "Connection": "keep-alive",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept": "*/*",
      "Accept": "application/json",
      "x-api-key": "987654",
      // "Authorization": AuthenticationUser.getbasicauth(),
    };
    var body2 = {
      "DriverId": 40,
      "QatarId":
          "/9j/4AAQSkZJRgABAQAAAQABAAD/4QM+RXhpZgAASUkqAAgAAAALAAABAwABAAAAEBIAAAEBAwABAAAAjA0AAA8BAgAFAAAAogAAABABAgAJAAAAqAAAABIBAwABAAAAAAAAABoBBQABAAAAkgAAABsBBQABAAAAmgAAACgBAwABAAAAAgAAADIBAgAUAAAAsgAAABMCAwABAAAAAQAAAGmHBAABAAAAxgAAAAAAAABIAAAAAQAAAEgAAAABAAAAT1BQTwAAT1BQTyBGMTcAADIwMjE6MDM6MTYgMTg6NTk6MjcAIQABAAIAAwAAAFI5OAACAAcAAwAAADEuMACaggUAAQAAAKICAACdggUAAQAAAJoCAAAiiAMAAQAAAAIAAAAniAMAAQAAALgIAAAAkAcABAAAADAyMTADkAIAFAAAAFgCAAAEkAIAFAAAAGwCAAABkQcABAAAAAECAwABkgUAAQAAAMICAAACkgUAAQAAAKoCAAADkgUAAQAAAMoCAAAEkgoAAQAAALICAAAFkgUAAQAAANICAAAHkgMAAQAAAAIAAAAJkgMAAQAAABAAAAAKkgUAAQAAALoCAAB8kgcAXAAAANoCAACGkgcABwAAAJICAACQkgIABgAAAIACAACRkgIABgAAAIYCAACSkgIABgAAAIwCAAAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAAAAAAAADoAQAAQAAAAAAAAAXogMAAQAAAAEAAAABowEAAQAAAAEAAAACpAMAAQAAAAAAAAADpAMAAQAAAAAAAAAFpAMAAQAAAAMAAAAGpAMAAQAAAAAAAAAAAAAAMjAyMTowMzoxNiAxODo1OToyNwAyMDIxOjAzOjE2IDE4OjU5OjI3ADg2MDAwADg2MDAwADg2MDAwAG9wcG9fMzQAFgAAAAoAAAA/S0wAAOH1BRYAAAAKAAAAAAAAAAYAAAAqDQAA6AMAAOEQAADoAwAA/f7//2QAAAAWAAAACgAAAHsiUGlGbGF",
      "QatarIdBack":
          "/9j/4AAQSkZJRgABAQAAAQABAAD/4QNERXhpZgAASUkqAAgAAAALAAABAwABAAAAEBIAAAEBAwABAAAAjA0AAA8BAgAFAAAAogAAABABAgAJAAAAqAAAABIBAwABAAAAAAAAABoBBQABAAAAkgAAABsBBQABAAAAmgAAACgBAwABAAAAAgAAADIBAgAUAAAAsgAAABMCAwABAAAAAQAAAGmHBAABAAAAxgAAAAAAAABIAAAAAQAAAEgAAAABAAAAT1BQTwAAT1BQTyBGMTcAADIwMjE6MDM6MTYgMTk6MDA6MTAAIQABAAIAAwAAAFI5OAACAAcAAwAAADEuMACaggUAAQAAAKgCAACdggUAAQAAAKACAAAiiAMAAQAAAAIAAAAniAMAAQAAANcIAAAAkAcABAAAADAyMTADkAIAFAAAAFgCAAAEkAIAFAAAAGwCAAABkQcABAAAAAECAwABkgUAAQAAAMgCAAACkgUAAQAAALACAAADkgUAAQAAANACAAAEkgoAAQAAALgCAAAFkgUAAQAAANgCAAAHkgMAAQAAAAIAAAAJkgMAAQAAABAAAAAKkgUAAQAAAMACAAB8kgcAXAAAAOACAACGkgcABwAAAJgCAACQkgIABwAAAIACAACRkgIABwAAAIgCAACSkgIABwAAAJACAAAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAAAAAAAADoAQAAQAAAAAAAAAXogMAAQAAAAEAAAABowEAAQAAAAEAAAACpAMAAQAAAAAAAAADpAMAAQAAAAAAAAAFpAMAAQAAAAMAAAAGpAMAAQAAAAAAAAAAAAAAMjAyMTowMzoxNiAxOTowMDoxMAAyMDIxOjAzOjE2IDE5OjAwOjEwADk2NjAwMAAAOTY2MDAwAAA5NjYwMDAAAG9wcG9fMzQAFgAAAAoAAAA/S0wAAOH1BRYAAAAKAAAAAAAAAAYAAAAqDQAA6AMAAOEQAADoAwAA3f7//2QAAAAWAAAACgAAAHs",
      "DrivingLicense":
          "/9j/4AAQSkZJRgABAQAAAQABAAD/4QNERXhpZgAASUkqAAgAAAALAAABAwABAAAAEBIAAAEBAwABAAAAjA0AAA8BAgAFAAAAogAAABABAgAJAAAAqAAAABIBAwABAAAAAAAAABoBBQABAAAAkgAAABsBBQABAAAAmgAAACgBAwABAAAAAgAAADIBAgAUAAAAsgAAABMCAwABAAAAAQAAAGmHBAABAAAAxgAAAAAAAABIAAAAAQAAAEgAAAABAAAAT1BQTwAAT1BQTyBGMTcAADIwMjE6MDM6MTYgMTk6MDA6MzEAIQABAAIAAwAAAFI5OAACAAcAAwAAADEuMACaggUAAQAAAKgCAACdggUAAQAAAKACAAAiiAMAAQAAAAIAAAAniAMAAQAAAJgBAAAAkAcABAAAADAyMTADkAIAFAAAAFgCAAAEkAIAFAAAAGwCAAABkQcABAAAAAECAwABkgUAAQAAAMgCAAACkgUAAQAAALACAAADkgUAAQAAANACAAAEkgoAAQAAALgCAAAFkgUAAQAAANgCAAAHkgMAAQAAAAIAAAAJkgMAAQAAABAAAAAKkgUAAQAAAMACAAB8kgcAWwAAAOACAACGkgcABwAAAJgCAACQkgIABwAAAIACAACRkgIABwAAAIgCAACSkgIABwAAAJACAAAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAAAAAAAADoAQAAQAAAAAAAAAXogMAAQAAAAEAAAABowEAAQAAAAEAAAACpAMAAQAAAAAAAAADpAMAAQAAAAAAAAAFpAMAAQAAAAMAAAAGpAMAAQAAAAAAAAAAAAAAMjAyMTowMzoxNiAxOTowMDozMQAyMDIxOjAzOjE2IDE5OjAwOjMxADg4ODAwMAAAODg4MDAwAAA4ODgwMDAAAG9wcG9fMzQAFgAAAAoAAACfJSYAAOH1BRYAAAAKAAAAAAAAAAYAAAAqDQAA6AMAAMkUAADoAwAABAAAAGQAAAAWAAAACgAAAHs",
      "DrivingLicenseBack":
          "9j/4AAQSkZJRgABAQAAAQABAAD/4QM+RXhpZgAASUkqAAgAAAALAAABAwABAAAAEBIAAAEBAwABAAAAjA0AAA8BAgAFAAAAogAAABABAgAJAAAAqAAAABIBAwABAAAAAAAAABoBBQABAAAAkgAAABsBBQABAAAAmgAAACgBAwABAAAAAgAAADIBAgAUAAAAsgAAABMCAwABAAAAAQAAAGmHBAABAAAAxgAAAAAAAABIAAAAAQAAAEgAAAABAAAAT1BQTwAAT1BQTyBGMTcAADIwMjE6MDM6MTYgMTk6MDE6MDAAIQABAAIAAwAAAFI5OAACAAcAAwAAADEuMACaggUAAQAAAKICAACdggUAAQAAAJoCAAAiiAMAAQAAAAIAAAAniAMAAQAAAJ8DAAAAkAcABAAAADAyMTADkAIAFAAAAFgCAAAEkAIAFAAAAGwCAAABkQcABAAAAAECAwABkgUAAQAAAMICAAACkgUAAQAAAKoCAAADkgUAAQAAAMoCAAAEkgoAAQAAALICAAAFkgUAAQAAANICAAAHkgMAAQAAAAIAAAAJkgMAAQAAABAAAAAKkgUAAQAAALoCAAB8kgcAXAAAANoCAACGkgcABwAAAJICAACQkgIABgAAAIACAACRkgIABgAAAIYCAACSkgIABgAAAIwCAAAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAAAAAAAADoAQAAQAAAAAAAAAXogMAAQAAAAEAAAABowEAAQAAAAEAAAACpAMAAQAAAAAAAAADpAMAAQAAAAAAAAAFpAMAAQAAAAMAAAAGpAMAAQAAAAAAAAAAAAAAMjAyMTowMzoxNiAxOTowMTowMAAyMDIxOjAzOjE2IDE5OjAxOjAwADk3MDAwADk3MDAwADk3MDAwAG9wcG9fMzQAFgAAAAoAAAAKlD8AAOH1BRYAAAAKAAAAAAAAAAYAAAAqDQAA6AMAAOgRAADoAwAAW////2QAAAAWAAAACgAAAHsiUGlGbGF",
      "Istimara":
          "/9j/4AAQSkZJRgABAQAAAQABAAD/4QNERXhpZgAASUkqAAgAAAALAAABAwABAAAAEBIAAAEBAwABAAAAjA0AAA8BAgAFAAAAogAAABABAgAJAAAAqAAAABIBAwABAAAAAAAAABoBBQABAAAAkgAAABsBBQABAAAAmgAAACgBAwABAAAAAgAAADIBAgAUAAAAsgAAABMCAwABAAAAAQAAAGmHBAABAAAAxgAAAAAAAABIAAAAAQAAAEgAAAABAAAAT1BQTwAAT1BQTyBGMTcAADIwMjE6MDM6MTYgMTk6MDE6MjYAIQABAAIAAwAAAFI5OAACAAcAAwAAADEuMACaggUAAQAAAKgCAACdggUAAQAAAKACAAAiiAMAAQAAAAIAAAAniAMAAQAAAKAEAAAAkAcABAAAADAyMTADkAIAFAAAAFgCAAAEkAIAFAAAAGwCAAABkQcABAAAAAECAwABkgUAAQAAAMgCAAACkgUAAQAAALACAAADkgUAAQAAANACAAAEkgoAAQAAALgCAAAFkgUAAQAAANgCAAAHkgMAAQAAAAIAAAAJkgMAAQAAABAAAAAKkgUAAQAAAMACAAB8kgcAXAAAAOACAACGkgcABwAAAJgCAACQkgIABwAAAIACAACRkgIABwAAAIgCAACSkgIABwAAAJACAAAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAAAAAAAADoAQAAQAAAAAAAAAXogMAAQAAAAEAAAABowEAAQAAAAEAAAACpAMAAQAAAAAAAAADpAMAAQAAAAAAAAAFpAMAAQAAAAMAAAAGpAMAAQAAAAAAAAAAAAAAMjAyMTowMzoxNiAxOTowMToyNgAyMDIxOjAzOjE2IDE5OjAxOjI2ADI3NDAwMAAAMjc0MDAwAAAyNzQwMDAAAG9wcG9fMzQAFgAAAAoAAAA/S0wAAOH1BRYAAAAKAAAAAAAAAAYAAAAqDQAA6AMAAOEQAADoAwAAGv///2QAAAAWAAAACgAAAHs",
      "IstimaraBack":
          "/9j/4AAQSkZJRgABAQAAAQABAAD/4QNERXhpZgAASUkqAAgAAAALAAABAwABAAAAEBIAAAEBAwABAAAAjA0AAA8BAgAFAAAAogAAABABAgAJAAAAqAAAABIBAwABAAAAAAAAABoBBQABAAAAkgAAABsBBQABAAAAmgAAACgBAwABAAAAAgAAADIBAgAUAAAAsgAAABMCAwABAAAAAQAAAGmHBAABAAAAxgAAAAAAAABIAAAAAQAAAEgAAAABAAAAT1BQTwAAT1BQTyBGMTcAADIwMjE6MDM6MTYgMTk6MDE6NTIAIQABAAIAAwAAAFI5OAACAAcAAwAAADEuMACaggUAAQAAAKgCAACdggUAAQAAAKACAAAiiAMAAQAAAAIAAAAniAMAAQAAADgDAAAAkAcABAAAADAyMTADkAIAFAAAAFgCAAAEkAIAFAAAAGwCAAABkQcABAAAAAECAwABkgUAAQAAAMgCAAACkgUAAQAAALACAAADkgUAAQAAANACAAAEkgoAAQAAALgCAAAFkgUAAQAAANgCAAAHkgMAAQAAAAIAAAAJkgMAAQAAABAAAAAKkgUAAQAAAMACAAB8kgcAWwAAAOACAACGkgcABwAAAJgCAACQkgIABwAAAIACAACRkgIABwAAAIgCAACSkgIABwAAAJACAAAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAAAAAAAADoAQAAQAAAAAAAAAXogMAAQAAAAEAAAABowEAAQAAAAEAAAACpAMAAQAAAAAAAAADpAMAAQAAAAAAAAAFpAMAAQAAAAMAAAAGpAMAAQAAAAAAAAAAAAAAMjAyMTowMzoxNiAxOTowMTo1MgAyMDIxOjAzOjE2IDE5OjAxOjUyADg1MDAwMAAAODUwMDAwAAA4NTAwMDAAAG9wcG9fMzQAFgAAAAoAAACfJSYAAOH1BRYAAAAKAAAAAAAAAAYAAAAqDQAA6AMAAMkUAADoAwAA5f///2QAAAAWAAAACgAAAHs",
      "VehicleName": "abcd",
      "ModalNo": "2017",
      "PlateNo": "123456",
      "VehicleType": "1"
    };
    print(driverId);
    print(vehicleName);
    print(modelNo);
    print(plateNo);
    print(typeId);
    print(qid);
    print(qidBack);
    print(dl);
    print(dlBack);
    print(istimara);
    print(istimaraBack);

    print("data body for signup: $data");
    print(webAPIKey);
    try {
      var res = await http.post(driverRegistrationNext,
          headers: webAPIKey, body: data);
      var response = json.decode(res.body);
      print("vehicle types: $response");
      print("vehicle types: ${res.statusCode}");
      if (res.statusCode == 200) {
        progressDialog.hide();

        return response;
      } else if (res.statusCode == 404) {
        progressDialog.hide();
        return response;
      } else {
        progressDialog.hide();
        BotToast.showText(
          text: response["Message"],
          duration: Duration(seconds: 2),
        );
        return response;
      }
    } on SocketException {
      progressDialog.hide();
      BotToast.showText(
        text: "No Internet Connection",
        duration: Duration(seconds: 2),
      );
    } on HttpException catch (error) {
      progressDialog.hide();
      print(error);
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    } on FormatException catch (error) {
      progressDialog.hide();
      print(error);

      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
      // EasyLoading.dismiss();
      print("Bad response format 👎");
    } catch (error) {
      progressDialog.hide();
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    }
  }

  Future sendMobile(mobile, BuildContext context) async {
    print(sendMobileNo);
    initialiseProgressDialog(context);
    progressDialog.show();
    String language = await getLanguage();
    var data = {
      "Mobile": "$mobile",
    };
    print("data body for signup: $data");
    print(webAPIKey);
    try {
      var res = await http.post(sendMobileNo, headers: webAPIKey, body: data);
      var response = json.decode(res.body);
      print("vehicle types: $response");
      if (res.statusCode == 200) {
        progressDialog.hide();
        return response;
      } else {
        progressDialog.hide();
        BotToast.showText(
          text: response["Message"],
          duration: Duration(seconds: 2),
        );
        return response;
      }
    } on SocketException {
      progressDialog.hide();
      BotToast.showText(
        text: "No Internet Connection",
        duration: Duration(seconds: 2),
      );
    } on HttpException catch (error) {
      progressDialog.hide();
      print(error);
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    } on FormatException catch (error) {
      progressDialog.hide();
      print(error);

      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
      // EasyLoading.dismiss();
      print("Bad response format 👎");
    } catch (error) {
      progressDialog.hide();
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    }
  }

  Future setProfile(
      firstName, lastName, mobile, password, BuildContext context) async {
    print(driverRegistration);
    initialiseProgressDialog(context);
    progressDialog.show();
    String language = await getLanguage();
    var data = {
      "FirstName": "$firstName",
      "LastName": "$lastName",
      "Mobile": "$mobile",
      "Password": "$password",
    };
    print("data body for signup: $data");
    print(webAPIKey);
    try {
      var res =
          await http.post(driverRegistration, headers: webAPIKey, body: data);
      var response = json.decode(res.body);
      print("vehicle types: $response");
      if (res.statusCode == 200) {
        progressDialog.hide();
        return response;
      } else {
        progressDialog.hide();
        BotToast.showText(
          text: response["Message"],
          duration: Duration(seconds: 2),
        );
        return response;
      }
    } on SocketException {
      progressDialog.hide();
      BotToast.showText(
        text: "No Internet Connection",
        duration: Duration(seconds: 2),
      );
    } on HttpException catch (error) {
      progressDialog.hide();
      print(error);
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    } on FormatException catch (error) {
      progressDialog.hide();
      print(error);

      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
      // EasyLoading.dismiss();
      print("Bad response format 👎");
    } catch (error) {
      progressDialog.hide();
      BotToast.showText(
        text: "$error",
        duration: Duration(seconds: 2),
      );
    }
  }

  chooseImage(bool selectImage) async {
    File _image;
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(
        source: selectImage == true ? ImageSource.gallery : ImageSource.camera,
        maxHeight: 400,
        maxWidth: 400);

    if (pickedFile != null) {
      _image = File(pickedFile.path);
      print(_image);
      return _image;
    } else {
      return null;
      print('No image selected.');
    }
    //                     .then((value) {
    //   print(value.path);
    //   if (value != null) {
    //     return value;
    //   } else {
    //     return null;
    //   }
    // });
    //print('Image file from gallery is $file ');
  }
}
