import 'dart:io';

import 'package:SathaCaptain/model/singleton.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/utilis/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:provider/provider.dart';

class APIHELPER {
  Future<dynamic> offlineUser(context) async {
    var header = {
      'Access-Control-Allow-Origin': "*",
      "Connection": "keep-alive",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept": "*/*",
      "Accept": "application/json"
      // "Authorization": AuthenticationUser.getbasicauth(),
    };

    try {
      if (Userss.userData.driverId != null) {
        print("driver id: ${Userss.userData.driverId}");

        // _dataProvider.setPushNotificationEnabled(true, context);
        var data = {
          "DriverId": Userss.userData.driverId,
          "Status": "Not Available"
        };
        print(data);
        var response =
            await http.post(updateStatusURL, headers: webAPIKey, body: data);
        if (response.statusCode == 200) {
          print("Status code:" + response.statusCode.toString());
          Provider.of<DriverProvider>(context, listen: false)
              .updateCurrentStatus("Not Available");
        } else {
          Provider.of<DriverProvider>(context, listen: false)
              .updateCurrentStatus("Not Available");
        }
        print(response);
      } else {
        // print("driver id: ${Userss.userData.driverId}");
      }
    } on SocketException {
      // constValues().toast("${getTranslated(context, "no_internet")}", context);

      print('No Internet connection 😑');
    } on HttpException catch (error) {
      print(error);

      // constValues().toast("$error", context);

      print("Couldn't find the post 😱");
    } on FormatException catch (error) {
      print(error);

      // constValues().toast("$error", context);

      print("Bad response format 👎");
    } catch (value) {
      // constValues().toast("$value",      EasyLoading.dismiss();
      print(value);
    }
  }
}
