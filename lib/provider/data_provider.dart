import 'dart:collection';
import 'dart:io';

import 'package:SathaCaptain/app_router.dart';
import 'package:SathaCaptain/generated/l10n.dart';

import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/model/trip_request.dart';
import 'package:SathaCaptain/provider/driver_provider.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/assets.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:provider/provider.dart';
import 'package:vibration/vibration.dart';

import '../language_constants.dart';

class DataProvider with ChangeNotifier {
  //FOR CHANGING APPLICATION'S LANGUAGE
  String _language;
  String get language => _language;
  changeLanguage(String language) {
    _language = language;
    notifyListeners();
    setLanguagePrefs(
      language: _language,
    );
  }

  Future checkSelectedLanguage() async {
    String lang = await getLanguage() ?? 'English';
    _language = lang;
    print(lang);
    notifyListeners();
  }

  String driverStatus = '';
  changeDriverStatus({@required String status}) async {
    driverStatus = status;
    notifyListeners();
  }

  TripRequest tripRequest = TripRequest();

  int _screenIndex;
  int get screenIndex => _screenIndex;

  setIndex(int i) {
    _screenIndex = i;
    notifyListeners();
  }

  bool _showPopUp;
  bool get showPopUp => _showPopUp;
  Map<String, dynamic> values;
  CountDownController countDownController;
  TextEditingController startDateTextEditingController,
      endDateTextEditingController;

  String startDate, endDate;
  changeStartDate(String val) {
    startDate = val;
    notifyListeners();
  }

  changeEndDate(String val) {
    endDate = val;
    notifyListeners();
  }

  var _navigationService = locator<NavigationService>();

  setShowPopUp(bool show, var data, BuildContext context) async {
    _showPopUp = show;
    if (show) {
      if (data != null) {
        var value = Platform.isAndroid
            ? LinkedHashMap.from(data['data'])
            : LinkedHashMap.from(data);
        if (value != null) {
          if (await Vibration.hasCustomVibrationsSupport()) {
            Vibration.vibrate(duration: 1000);
          } else {
            Vibration.vibrate();
            await Future.delayed(Duration(milliseconds: 500));
            Vibration.vibrate();
          }

          tripRequest = TripRequest.fromLinkedHashMap(value);

          print(tripRequest);
          setTripId(tripRequest.tripCartId);

          _navigationService.navigateTo(AppRoute.recentTripScreen);
        }
        if (value.length == 0) {
          changeTripStatus('No Trip');
          _navigationService.navigateTo(AppRoute.mainScreen);
        }
        print(tripRequest);
      }
      Future.delayed(
          Duration(
            seconds: 3,
          ), () {
        countDownController.resume();
      });
    } else {
      print("not Working");
    }
    notifyListeners();
  }

  bool _showBottomSheet;
  bool get showBottomSheet => _showBottomSheet;

  setShowBottomSheetAppearance(bool show) {
    _showBottomSheet = show;
    notifyListeners();
  }

  //SHOW DRAWER AND HIDE APPBAR
  bool _showEndDrawer;
  bool get showEndDrawer => _showEndDrawer;

  showEndDrawerEnabled(bool show) {
    _showEndDrawer = show;
    notifyListeners();
  }

  String tripStatus;
  changeTripStatus(String status) {
    tripStatus = status;
    notifyListeners();
  }

  //TURN ON PUSH NOTIFICATION WHEN DRIVER GO LIVE.
  bool _isPushNotificationEnabled;
  bool get isPushNotificationEnabled => _isPushNotificationEnabled;

  setPushNotificationEnabled(bool enable, BuildContext context) async {
    _isPushNotificationEnabled = enable;
    if (_isPushNotificationEnabled) {
      PushNotificationService pushNotificationService =
          PushNotificationService();
      pushNotificationService.init(context: context);
    }
  }

  bool isInternetAvailable;
  changeInternetAvailability(bool available) {
    isInternetAvailable = available;
    notifyListeners();
  }

  Future showLocalNotification(
    String message,
  ) async {
    print(message);
  }

  init() {
    tripStatus = 'Waiting for trip...';
    _language = "Arabic";
    _showEndDrawer = false;
    _screenIndex = 0;
    isInternetAvailable = true;
    _isPushNotificationEnabled = false;
    _showPopUp = false;
    _showBottomSheet = false;
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String formatted = formatter.format(now);
    countDownController = CountDownController();
    startDate = formatted;
    endDate = formatted;
  }

  Widget cupertino = SizedBox();
  changeWidget(Widget widget) {
    cupertino = widget;
    notifyListeners();
  }

  Widget recentScreen;
  changeRecentScreen(Widget screen) {
    recentScreen = screen;
    notifyListeners();
  }

  Widget tripDetailsWidget;
  changeTripDetailsWidget(Widget widget) {
    tripDetailsWidget = widget;
    notifyListeners();
  }

  Widget notificationWidget;
  changeNotificationWidget(Widget widget) {
    notificationWidget = widget;
    notifyListeners();
  }

  Widget itemPickedWidget;
  changeItemPickedWidget(Widget widget) {
    itemPickedWidget = widget;
    notifyListeners();
  }

  Widget pickUpAddressWidget;
  changePickupAddressWidget(Widget widget) {
    pickUpAddressWidget = widget;
    notifyListeners();
  }

  Widget itemPickedWidget2;
  changeItemPickedWidget2(Widget widget) {
    itemPickedWidget2 = widget;
    notifyListeners();
  }

  Widget dropOffAddressWidget;
  changeDropOffAddressWidget(Widget widget) {
    dropOffAddressWidget = widget;
    notifyListeners();
  }

  DataProvider() {
    recentScreen = Scaffold(
      body: Container(),
    );
    notificationWidget = SizedBox();
    tripDetailsWidget = SizedBox();
    cupertino = SizedBox();
    itemPickedWidget = SvgPicture.asset(
      warning,
      fit: BoxFit.fitWidth,
    );
    itemPickedWidget2 = SvgPicture.asset(
      warning,
      fit: BoxFit.fitWidth,
    );
    checkSelectedLanguage();

    init();
  }

//DROP OFF BOTTOM SHEET FUNCTION
  Future dropOffFunction(BuildContext context) async {
    List<String> allMapIcons = [];
    List<String> allMaps = [];
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _driverProvider = Provider.of<DriverProvider>(context, listen: false);
    final availableMaps = await MapLauncher.installedMaps;

    availableMaps.forEach((element) {
      allMapIcons.add(element.icon);
      allMaps.add(element.mapName);
    });
    if (availableMaps.length != 0) {
      showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  color: greyColorDark,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: height * .01,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Spacer(),
                        Container(
                          width: width * .07,
                          height: height * .005,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(
                                20,
                              )),
                        ),
                        Spacer(),
                      ],
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 12,
                      ),
                      child: Text(
                        // S.of(context).openWith,
                        getTranslated(context, 'openWith'),

                        style: tripStatusTextStyle.copyWith(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 12,
                      ),
                      child: Wrap(
                        spacing: width * .18,
                        children: List.generate(
                          availableMaps.length,
                          (index) {
                            return InkWell(
                              onTap: () {
                                setShowPopUp(false, {}, context);
                                changeTripStatus('Trip started.');
                                availableMaps[index].showMarker(
                                  coords: Coords(
                                    _driverProvider.tripDetailss != null
                                        ? double.parse(
                                            _driverProvider
                                                .tripDetailss.dropLat,
                                          )
                                        : 31.621113,
                                    _driverProvider.tripDetailss != null
                                        ? double.parse(
                                            _driverProvider
                                                .tripDetailss.dropLong,
                                          )
                                        : 74.282364,
                                  ),
                                  title: 'Pick Up Location',
                                );
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: height * .01,
                                  ),
                                  SizedBox(
                                    width: 24,
                                    height: 24,
                                    child: SvgPicture.asset(
                                      availableMaps[index].icon,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(
                                    height: height * .01,
                                  ),
                                  Text(
                                    allMaps[index],
                                    style: nameTextStyle.copyWith(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * .03,
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      );
    }
  }

//PICK UP BOTTOM SHEET
  Future pickUpFunction(BuildContext context) async {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var _driverProvider = Provider.of<DriverProvider>(context, listen: false);

    List<String> allMapIcons = [];
    List<String> allMaps = [];

    final availableMaps = await MapLauncher.installedMaps;

    availableMaps.forEach((element) {
      allMapIcons.add(element.icon);
      allMaps.add(element.mapName);
    });
    if (availableMaps.length != 0) {
      showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  color: greyColorDark,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: height * .01,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Spacer(),
                        Container(
                          width: width * .07,
                          height: height * .005,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(
                                20,
                              )),
                        ),
                        Spacer(),
                      ],
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 12,
                      ),
                      child: Text(
                        // S.of(context).openWith,
                        getTranslated(context, 'openWith'),

                        style: tripStatusTextStyle.copyWith(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 12,
                      ),
                      child: Wrap(
                        spacing: width * .18,
                        children: List.generate(
                          availableMaps.length,
                          (index) {
                            return InkWell(
                              onTap: () {
                                setShowPopUp(false, {}, context);
                                changeTripStatus('Trip started.');
                                availableMaps[index].showMarker(
                                  coords: Coords(
                                    _driverProvider.tripDetailss != null
                                        ? double.parse(
                                            _driverProvider
                                                .tripDetailss.startingLat,
                                          )
                                        : 31.621113,
                                    _driverProvider.tripDetailss != null
                                        ? double.parse(
                                            _driverProvider
                                                .tripDetailss.startingLong,
                                          )
                                        : 74.282364,
                                  ),
                                  title: 'Pick Up Location',
                                );
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: height * .01,
                                  ),
                                  SizedBox(
                                    width: 24,
                                    height: 24,
                                    child: SvgPicture.asset(
                                      availableMaps[index].icon,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(
                                    height: height * .01,
                                  ),
                                  Text(
                                    allMaps[index],
                                    style: nameTextStyle.copyWith(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * .03,
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      );
    }
  }
}
