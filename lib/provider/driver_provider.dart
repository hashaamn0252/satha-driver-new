import 'dart:convert';
import 'dart:io';

import 'package:SathaCaptain/generated/l10n.dart';
import 'package:SathaCaptain/locator.dart';
import 'package:SathaCaptain/model/driver.dart';
import 'package:SathaCaptain/model/singleton.dart';
import 'package:SathaCaptain/model/trip_details.dart';
import 'package:SathaCaptain/model/userdatamodel.dart';
import 'package:SathaCaptain/provider/data_provider.dart';
import 'package:SathaCaptain/provider/mapbox_location_provider.dart';
import 'package:SathaCaptain/screens/signupOTP.dart';
import 'package:SathaCaptain/screens/userprofile.dart';
import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/services/navigation_service.dart';
import 'package:SathaCaptain/utilis/colors.dart';
import 'package:SathaCaptain/utilis/constants.dart';
import 'package:SathaCaptain/utilis/device_info.dart';
import 'package:SathaCaptain/utilis/globals.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:SathaCaptain/utilis/routes.dart';
import 'package:SathaCaptain/utilis/styles.dart';
import 'package:SathaCaptain/widgets/trip_history.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_restart/flutter_restart.dart';
import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import '../app_router.dart';
import 'package:SathaCaptain/screens/main_screen.dart';

class DriverProvider with ChangeNotifier {
  //USERNAME AND PASSWORD TEXTEDITING CONTROLLERS FOR INSERTING UESR CREDIENTIALS IN TEXTFIELD
  TextEditingController userNameTextEditingController,
      passwordTextEditingController,
      forgotPasswordController,
      otpTextEditingController,
      newPasswordTextEditingController,
      phoneNumberTextEditingController,
      confirmPasswordTextEditingController;

  String distance, price;

  String _currentTripStatus;
  String get currentTripStatus => _currentTripStatus;

  checkCurrentTripStatus(String status) {
    _currentTripStatus = status;
    notifyListeners();
  }

  String driverStatus;
  changeDriverStatus(String status) {
    driverStatus = status;
    notifyListeners();
  }

  clear() {
    userNameTextEditingController.text = '';
    passwordTextEditingController.text = '';
    forgotPasswordController.text = '';
    otpTextEditingController.text = '';
    newPasswordTextEditingController.text = '';
    phoneNumberTextEditingController.text = '';
    confirmPasswordTextEditingController.text = '';
  }

  int otpCode;
  NavigationService _navigationService = locator<NavigationService>();

//USER MODEL FOR FOR USER DATA WHICH WILL BE RECEIVED FROM THE API CALL
  Driver _driver;
  Driver get driver => _driver;
  Driver changeDriverDetails(Map data) {
    _driver = data != null ? Driver.fromMap(data) : Driver();
    notifyListeners();
  }

  bool _isVehiclePicked;
  bool get isVehicledPicked => _isVehiclePicked;

  setIsVehiclePicked(bool picked) {
    _isVehiclePicked = picked;
    notifyListeners();
  }

  //TRIP REQUEST MODEL FOR SENDING THE RESPONSE

  //GETTING TRIP DETAILS
  TripDetails tripDetails;
  TripDetails get tripDetailss => tripDetails;

  changeTripDetails(var details) {
    tripDetails = TripDetails.fromMap(details);
    notifyListeners();
  }

//PROGRESS INDICATOR
  ProgressDialog progressDialog;

  initialiseProgressDialog(BuildContext context) {
    progressDialog = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
    );
    progressDialog.style(
        message: 'Please wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: tripStatusTextStyle.copyWith(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: tripStatusTextStyle.copyWith(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
  }

  // PushNotificationService pushNotificationService;

  //USER SIGN IN
  Future signIn({
    @required String mobile,
    @required String password,
    @required BuildContext context,
  }) async {
    initialiseProgressDialog(context);
    progressDialog.show();
    SystemInfo systemInfo = SystemInfo();
    await systemInfo.deviceInfo();
    String deviceInfo = await getDeviceInfo();
    print(loginURL);
    print({
      "Mobile": mobile,
      "Password": password,
    });
    if (deviceInfo != null) {
      await http.post(loginURL, headers: webAPIKey, body: {
        "Mobile": mobile,
        "Password": password,
      }).catchError((err) {
        print('Error in Sign in =====>>>>>. $err');
      }).then(
        (value) async {
          var response = json.decode(value.body);
          print("sign in respoinse: $response");
          if (response["Message"]["EnResponse"].contains("Successfully")) {
            progressDialog.hide();
            if (_driver.systemId == deviceInfo) {
              if (int.parse(
                      response['Response']['ProfileComplete'].toString()) ==
                  1) {
                if (int.parse(
                        response['Response']['ApprovalStatus'].toString()) ==
                    0) {
                  BotToast.showText(
                    text: "Please wait for the approval of account. Thank You!",
                    duration: Duration(seconds: 2),
                  );
                } else {
                  updateDeviceToken(response['Response']['Id'].toString());

                  changeDriverDetails(response['Response']);
                  setUserLoginPrefs(
                      userId: response['Response']['Id'].toString());
                  checkTripStatus(context);
                  _navigationService.navigateAndRemoveUntil(
                    AppRoute.mainScreen,
                  );
                }
              } else {
                User.userData.mobile = response['Response']['Id'].toString();
                _navigationService.navigateTo(
                  AppRoute.sigup,
                );

                // AppRoutes.replace(context, SetNewPasswordScreen());
              }
              // _navigationService.navigateAndRemoveUntil(
              //   AppRoute.mainScreen,
              // );
            } else {
              // pushNotificationService.init(context: context);

              bool er = false;
              await http
                  .post(loginURL,
                      headers: webAPIKey,
                      body: {"Mobile": mobile, "Password": password})
                  .catchError((err) => er = true)
                  .then(
                    (value) async {
                      var result = json.decode(value.body);
                      if (!er) {
                        if (result["Message"]["EnResponse"]
                            .contains("Successfully")) {
                          progressDialog.hide();
                          // _driver = Driver.fromMap(response['Response']);
                          if (int.parse(response['Response']['ProfileComplete']
                                  .toString()) ==
                              1) {
                            if (int.parse(response['Response']['ApprovalStatus']
                                    .toString()) ==
                                0) {
                              BotToast.showText(
                                text:
                                    "Please wait for the approval of account. Thank You!",
                                duration: Duration(seconds: 2),
                              );
                            } else {
                              changeDriverDetails(response['Response']);
                              updateDeviceToken(
                                  response['Response']['Id'].toString());

                              updateSystemId();
                              setUserLoginPrefs(userId: _driver.id.toString());
                              progressDialog.hide();
                              checkTripStatus(context);

                              _navigationService.navigateAndRemoveUntil(
                                AppRoute.mainScreen,
                              );
                            }
                          } else {
                            progressDialog.hide();
                            User.userData.mobile =
                                response['Response']['Id'].toString();
                            _navigationService.navigateTo(
                              AppRoute.sigup,
                            );
                            // AppRoutes.replace(context, SetNewPasswordScreen());
                          }
                        } else {
                          progressDialog.hide();
                          String lang = await getLanguage();

                          if (lang.toLowerCase() == "English".toLowerCase()) {
                            BotToast.showText(
                                text: result["Message"]["EnResponse"]);
                          } else {
                            BotToast.showText(
                                text: result["Message"]["ArResponse"]);
                          }
                        }
                      }
                    },
                  );
            }
          } else {
            progressDialog.hide();
            String lang = await getLanguage();

            if (lang != null) {
              if (lang.toLowerCase() == "English".toLowerCase()) {
                BotToast.showText(
                  text: response["Message"]["EnResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              } else {
                BotToast.showText(
                  text: response["Message"]["ArResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              }
            } else {
              BotToast.showText(
                text: response["Message"]["EnResponse"],
                textStyle: tripStatusTextStyle.copyWith(
                  fontSize: 12,
                  color: Colors.white,
                ),
                contentColor: Colors.red,
              );
            }
            // clearPrefs();
          }
        },
      );
    }
  }

  bool isButtonPressed = false;
  changeIsButtonPressed(bool press) {
    isButtonPressed = press;
    notifyListeners();
  }

  Widget tripHistroyWidget = SizedBox();
  changeTripHistoryWidget(Widget widget) {
    tripHistroyWidget = widget;
    notifyListeners();
  }

  Future cashReceived() async {
    bool _isError = false;
    if (tripDetails != null) {
      var body = {"TripId": tripDetails.tripId.toString()};
      http
          .post(
        cashReceivedURL,
        headers: webAPIKey,
        body: body,
      )
          .catchError((err) {
        print("Error ======>>>> $err");
        _isError = true;
      }).then((value) {
        if (!_isError) {
          var response = json.decode(value.body);
          print("payment received resposne: $response");
          if (response['Status'] == true) {
            updateCurrentStatus("AVAILABLE");
            FlutterRestart.restartApp();
          }
        }
      });
    }
  }

  List allTripDetails;
  List fromAddress;
  List toAddress;
  //DRIVER'S TRIP HISTORY
  Future driverHistory(
      String startingDate, String endingDate, BuildContext context) async {
    String driverId = await getUserIdPrefs();

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    changeIsButtonPressed(false);
    if (startingDate == null) {
      startingDate =
          Provider.of<DataProvider>(context, listen: false).startDate;
      endingDate = Provider.of<DataProvider>(context, listen: false).endDate;
    }
    var data = {
      "DriverId": driverId,
      "StartDate": startingDate ?? '',
      "EndDate": endingDate ?? '',
    };
    print("sdabd jbfbsaf: $data");
    if (driverId != null) {
      var apiCall = await http
          .post(tripHistory, headers: webAPIKey, body: data)
          .catchError(
            (err) => print('Error in trip history ======>>>>>> $err'),
          )
          .then(
        (value) async {
          var response = json.decode(value.body);
          checkCurrentTripStatus('No Trip');
          changeDriverStatus('Available');

          allTripDetails = [];
          fromAddress = [];
          toAddress = [];
          print("history response: ${response["Response"]}");
          print("dsabnkekwuhrfuwehtew: $allTripDetails");
          var re = response["Response"].length;
          print("lenghttttttttttttt: $re");
          for (int i = 0; i < re; i++) {
            allTripDetails.add(response["Response"][i]);
          }
          // .forEach(
          //   (element) {

          //   },
          // );
          print("sdalkndnsalsafjkesbfkwbesbtgfe: $allTripDetails");
          for (int i = 0; i < allTripDetails.length; i++) {
            if (allTripDetails[i]['StartingLat'] != null) {
              final coordinatesForPickup = Coordinates(
                double.parse(
                  allTripDetails[i]['StartingLat'].toString(),
                ),
                double.parse(
                  allTripDetails[i]['StartingLong'].toString(),
                ),
              );
              await Geocoder.local
                  .findAddressesFromCoordinates(coordinatesForPickup)
                  .then((value) {
                fromAddress.add(value.first.addressLine);
              });
              if (allTripDetails[i]['DropLat'] != null) {
                final coordinatesforDropOff = Coordinates(
                  double.parse(
                    allTripDetails[i]['DropLat'].toString(),
                  ),
                  double.parse(
                    allTripDetails[i]['DropLong'].toString(),
                  ),
                );
                await Geocoder.local
                    .findAddressesFromCoordinates(coordinatesforDropOff)
                    .then((value) {
                  print("sadsafdsa: $value");
                  toAddress.add(value.first.addressLine);
                });
              }
            } else {
              print("hello baby");
            }
          }

          Future.delayed(Duration(milliseconds: 1000), () {
            print(fromAddress);
            changeTripHistoryWidget(
              allTripDetails.length != 0
                  ? Expanded(
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: allTripDetails.length,
                        itemBuilder: (BuildContext context, index) {
                          return TripHistory(
                            pickupAddress: fromAddress[index],
                            dropOffAddress: toAddress[index],
                            tripId: allTripDetails[index]['TripId'].toString(),
                            startDate:
                                allTripDetails[index]['TripDate'].toString(),
                            totalAmount:
                                allTripDetails[index]['Price'].toString(),
                            totalDistance:
                                allTripDetails[index]['Distance'].toString(),
                          );
                        },
                      ),
                    )
                  : Column(
                      children: [
                        SizedBox(
                          height: height * .18,
                        ),
                        Text(
                          'No Trip Found',
                          style: tripStatusTextStyle,
                        ),
                      ],
                    ),
            );
            changeIsButtonPressed(true);
          });
        },
      );
    }
  }

  //GET DRIVER DETAILS IF DRIVER DEVICE INFO IS CHANGE THEN LOGOUT THE DRIVER FROM LAST DEVICE
  Future logout() async {
    bool _isError = false;
    String deviceInfo = await getDeviceInfo();
    String driverId = await getUserIdPrefs();
    if (deviceInfo != null && driverId != null) {
      var body = {"DriverId": driverId};
      await http
          .post(
            driverDetailsURL,
            headers: webAPIKey,
            body: body,
          )
          .catchError(
            (err) => _isError = true,
          )
          .then(
        (value) {
          if (!_isError) {
            var response = json.decode(value.body);
            // response['Response'];
            // _driver = Driver.fromMap(response['Response']);
            changeDriverDetails(response['Response']);

            if (_driver.systemId == deviceInfo) {
              //IF SAME THEN DO NOTHING
            } else {
              //IF DEVICE INFO AND SYSTEM ID NOT SAME THEN LOGOUT FROM APP
              clearPrefs();
              _navigationService.navigateAndRemoveUntil(
                AppRoute.loginScreen,
              );
            }
          }
        },
      );
    }
  }

  //PICKUP VEHICLE
  Future pickUpVehicle(
    BuildContext context,
  ) async {
    bool _isError = false;
    String tripId = await getTripId();
    String driverId = await getUserIdPrefs();
    if (tripId != null) {
      var data = {
        "TripId": tripId,
        "DriverId": driverId,
      };
      await http
          .post(
            pickUpURL,
            headers: webAPIKey,
            body: data,
          )
          .catchError((err) => {
                _isError = true,
              })
          .then((value) async {
        if (!_isError) {
          var response = json.decode(value.body);
          print(response);

          changeTripDetails(response['Response']);
          checkCurrentTripStatus(tripDetails.tripStatus);
          changeDriverStatus(tripDetails.driverStatus);
          print(tripDetails.tripId);
          final coordinatesForPickup = Coordinates(
            double.parse(
              tripDetails.startingLat,
            ),
            double.parse(
              tripDetails.startingLong,
            ),
          );
          var addresses = await Geocoder.local
              .findAddressesFromCoordinates(coordinatesForPickup);
          changePickUpAddress(
            addresses.first.addressLine,
          );
          final coordinatesforDropOff = Coordinates(
            double.parse(
              tripDetails.dropLat,
            ),
            double.parse(
              tripDetails.dropLong,
            ),
          );
          var address = await Geocoder.local
              .findAddressesFromCoordinates(coordinatesforDropOff);
          changeDropOffAddress(address.first.addressLine);
        } else {}
      });
    }
  }

  String newToken;
  changeToken(String token) {
    newToken = token;
    notifyListeners();
  }

  Future updateDeviceToken(String driverId) async {
    String deviceToken = await getDeviceToken();
    if (deviceToken != null) {
      changeToken(deviceToken);
    } else {
      setDeviceToken(newToken);
    }
    print(deviceToken);
    await http.post(updateDeviceTokenURL, headers: webAPIKey, body: {
      "DriverId": driverId,
      "DeviceToken": newToken,
    }).then((value) {
      var response = json.decode(value.body);
      print(response);
    });
  }

  //FORGOT PASSWORD
  Future forgotPassword(
      BuildContext context, String phoneNumber, screen) async {
    bool _isError = false;
    initialiseProgressDialog(context);
    progressDialog.show();
    var data = {"Mobile": phoneNumber};
    String language = await getLanguage();
    if (phoneNumber != null || phoneNumber.isNotEmpty) {
      await http
          .post(
            forgotPasswordURL,
            headers: webAPIKey,
            body: data,
          )
          .catchError((err) => _isError = true)
          .then((value) async {
        var result = json.decode(value.body);
        print(result);
        if (!_isError) {
          if (result["Message"]["EnResponse"].contains("Send")) {
            // BotToast.showText(
            //   text: result["Data"]["EnResponse"].toString(),
            //   duration: Duration(seconds: 2),
            // );
            _driver.id = result["Data"]["DriverId"];
            otpCode = result["Data"]["Pin"];
            progressDialog.hide();
            screen == 0
                ? _navigationService.navigateTo(AppRoute.otpScreen)
                : AppRoutes.push(
                    context,
                    SignUpOTP(
                      OTP: otpCode,
                    ));
            ;
            clear();
          } else {
            progressDialog.hide();

            // BotToast.showText(
            //   text: result["Data"]["Pin"].toString(),
            //   duration: Duration(seconds: 2),
            // );
            String lang = await getLanguage();

            if (lang != null) {
              if (lang.toLowerCase() == "English".toLowerCase()) {
                BotToast.showText(
                  text: result["Message"]["EnResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              } else {
                BotToast.showText(
                  text: result["Message"]["ArResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              }
            } else {
              BotToast.showText(
                text: result["Message"]["EnResponse"],
                textStyle: tripStatusTextStyle.copyWith(
                  fontSize: 12,
                  color: Colors.white,
                ),
                contentColor: Colors.red,
              );
            }
            clear();
          }
        } else {
          progressDialog.hide();

          BotToast.showText(
            text: 'Something went wrong.',
            duration: Duration(seconds: 2),
          );
        }
      });
    } else {
      progressDialog.hide();
    }
  }

//sign Up
  Future signUp(firstName, lastName, userName, address, phoneNo, password,
      vehicleName, plateNo, qid, dl, BuildContext context) async {
    bool isError = false;
    initialiseProgressDialog(context);
    progressDialog.show();
    String language = await getLanguage();
    if (driver != null && (driver.id != null || driver.id != 0)) {
      var data = {
        "First Name": driver.id.toString(),
        "Last Name": firstName,
        "Mobile": firstName,
        "User Name": firstName,
        "Password": firstName,
        "Address": firstName,
        "Qatar Id": firstName,
        "Driving License": firstName,
        "Vehicle Type": firstName,
        "Vehicle Name": firstName,
        "Modal Number": firstName,
        "Plate Number": firstName,
      };
      await http
          .post(updatePasswordURL, headers: webAPIKey, body: data)
          .catchError((err) => isError = true)
          .then((value) async {
        var response = json.decode(value.body);
        if (!isError) {
          if (response["Message"]["EnResponse"].contains("Updated")) {
            progressDialog.hide();
            if (language == "English") {
              BotToast.showText(
                text: response["Message"]["EnResponse"],
                duration: Duration(seconds: 2),
              );
            } else {
              BotToast.showText(
                text: response["Message"]["ArResponse"],
                duration: Duration(seconds: 2),
              );
            }
            clear();

            _navigationService.navigateAndRemoveUntil(AppRoute.loginScreen);
          } else {
            progressDialog.hide();
            String lang = await getLanguage();

            if (lang != null) {
              if (lang.toLowerCase() == "English".toLowerCase()) {
                BotToast.showText(
                  text: response["Message"]["EnResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              } else {
                BotToast.showText(
                  text: response["Message"]["ArResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              }
            } else {
              BotToast.showText(
                text: response["Message"]["EnResponse"],
                textStyle: tripStatusTextStyle.copyWith(
                  fontSize: 12,
                  color: Colors.white,
                ),
                contentColor: Colors.red,
              );
            }
          }
        } else {
          progressDialog.hide();
        }
      });
    }
  }

  //UPDATE PASSWORD

  //UPDATE PASSWORD
  Future updatePassword(String newPassword, BuildContext context) async {
    bool isError = false;
    initialiseProgressDialog(context);
    progressDialog.show();
    String language = await getLanguage();
    if (driver != null && (driver.id != null || driver.id != 0)) {
      var data = {"DriverId": driver.id.toString(), "Password": newPassword};
      await http
          .post(updatePasswordURL, headers: webAPIKey, body: data)
          .catchError((err) => isError = true)
          .then((value) async {
        var response = json.decode(value.body);
        if (!isError) {
          if (response["Message"]["EnResponse"].contains("Updated")) {
            progressDialog.hide();
            if (language == "English") {
              BotToast.showText(
                text: response["Message"]["EnResponse"],
                duration: Duration(seconds: 2),
              );
            } else {
              BotToast.showText(
                text: response["Message"]["ArResponse"],
                duration: Duration(seconds: 2),
              );
            }
            clear();

            _navigationService.navigateAndRemoveUntil(AppRoute.loginScreen);
          } else {
            progressDialog.hide();
            String lang = await getLanguage();

            if (lang != null) {
              if (lang.toLowerCase() == "English".toLowerCase()) {
                BotToast.showText(
                  text: response["Message"]["EnResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              } else {
                BotToast.showText(
                  text: response["Message"]["ArResponse"],
                  textStyle: tripStatusTextStyle.copyWith(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  contentColor: Colors.red,
                );
              }
            } else {
              BotToast.showText(
                text: response["Message"]["EnResponse"],
                textStyle: tripStatusTextStyle.copyWith(
                  fontSize: 12,
                  color: Colors.white,
                ),
                contentColor: Colors.red,
              );
            }
          }
        } else {
          progressDialog.hide();
        }
      });
    }
  }

  //CHECK CURRENT USER LOGIN OR NOT
  Future checkLogin(BuildContext context) async {
    bool _isError = false;
    String userId = await getUserIdPrefs();
    String deviceInfo = await getDeviceInfo();
    if (userId != null) {
      Userss.userData.driverId = userId;
      print("userId: ${Userss.userData.driverId}");
      await http.post(driverDetailsURL,
          headers: webAPIKey, body: {"DriverId": userId}).then((value) async {
        if (!_isError) {
          var response = json.decode(value.body);
          print("profile: $response");
          if (response["Status"]) {
            _driver = Driver.fromMap(response["Response"]);
            updateDeviceToken(userId);
            var _dataProvider =
                Provider.of<DataProvider>(context, listen: false);
            _dataProvider.setPushNotificationEnabled(true, context);

            if (_driver.systemId == deviceInfo) {
              checkTripStatus(context);
              if (response['Response']['Availability'].toString() ==
                  "AVAILABLE") {
                // _currentTripStatus = '';
                Provider.of<MapBoxLocationProvider>(context, listen: false)
                    .changeGoLive(true, context);

                _navigationService.navigateAndRemoveUntil(AppRoute.mainScreen);
              } else {
                // PushNotificationService pushNotificationService =
                //     PushNotificationService();
                // pushNotificationService.init(context: context);
                _navigationService.navigateAndRemoveUntil(AppRoute.mainScreen);
              }
              // Provider.of<DriverProvider>(context, listen: false)
              //     .updateCurrentStatus("Not Available");

              // _navigationService.navigateAndRemoveUntil(AppRoute.mainScreen);
            } else {
              _navigationService.navigateAndRemoveUntil(AppRoute.loginScreen);
            }
          } else {
            _navigationService.navigateAndRemoveUntil(AppRoute.loginScreen);
          }
        } else {
          _navigationService.navigateAndRemoveUntil(AppRoute.loginScreen);
        }
      });
    } else {
      _navigationService.navigateAndRemoveUntil(AppRoute.loginScreen);
    }
  }

//UPDATE CURRENT LOCATION
  Future updateCurrentLatLng(String lat, String long) async {
    if (_driver != null) {
      var data = {
        "DriverId": _driver.id.toString(),
        "Latitude": lat,
        "Longitude": long,
      };
      await http
          .post(updateCurrentLatLngURL, headers: webAPIKey, body: data)
          .then((value) {
        var response = json.decode(value.body);
        print(response);
      });
    }
  }

//UPDATE SYSTEM INFO
  Future updateSystemId() async {
    bool _isError = false;
    String driverId = _driver.id.toString();
    String message = '';
    String systemId;
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo =
            await deviceInfoPlugin.androidInfo;
        systemId = androidDeviceInfo.androidId +
            androidDeviceInfo.fingerprint +
            androidDeviceInfo.board;
      } else if (Platform.isIOS) {
        IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
        systemId = iosDeviceInfo.identifierForVendor +
            iosDeviceInfo.utsname.machine +
            iosDeviceInfo.utsname.nodename;
      }
    } on PlatformException {
      message = 'Error found';
      print(message);
    }
    if (driverId != null && systemId != null) {
      var body = {
        "DriverId": driverId,
        "SystemId": systemId,
      };
      await http
          .post(
            updateSystemIdURL,
            headers: webAPIKey,
            body: body,
          )
          .catchError((err) => _isError = true)
          .then(
        (value) {
          if (!_isError) {
            print("${json.decode(value.body)}");
          } else {
            print('Error found');
          }
        },
      );
    }
  }

  String pickUpAddress;
  String dropOffAddress;
  changePickUpAddress(String address) {
    pickUpAddress = address;
    notifyListeners();
  }

  changeDropOffAddress(String address) {
    dropOffAddress = address;
    notifyListeners();
  }

  //DRIVER RESPONSE WHEN REQUEST ARRIVED
  Future driverResponse(
      {@required String response, @required BuildContext context}) async {
    bool _isError = false;
    String driverId = _driver.id.toString();
    print("driver id: $driverId");
    if (Provider.of<DataProvider>(context, listen: false).tripRequest != null) {
      if (driverId ==
          Provider.of<DataProvider>(context, listen: false)
              .tripRequest
              .driverId
              .toString()) {
        var data = {
          "TripCartId": Provider.of<DataProvider>(context, listen: false)
              .tripRequest
              .tripCartId,
          "RequestId": Provider.of<DataProvider>(context, listen: false)
              .tripRequest
              .requestId,
          "DriverId": Provider.of<DataProvider>(context, listen: false)
              .tripRequest
              .driverId,
          "DriverStatus": response
        };
        print(data);
        await http
            .post(driverResponseURL, headers: webAPIKey, body: data)
            .catchError((err) => _isError = true)
            .then((value) async {
          if (!_isError) {
            var response = json.decode(value.body);
            print(response);
            if (response["Message"].contains("Accepted")) {
              var tripData = response["Response"];
              changeTripDetails(tripData);
              checkCurrentTripStatus(tripDetails.tripStatus);
              changeDriverStatus(tripDetails.driverStatus);

              print(tripDetails);
              setTripId(tripDetails.tripId.toString());
              final coordinatesForPickup = Coordinates(
                double.parse(
                  tripDetails.startingLat,
                ),
                double.parse(
                  tripDetails.startingLong,
                ),
              );
              var addresses = await Geocoder.local
                  .findAddressesFromCoordinates(coordinatesForPickup);
              changePickUpAddress(
                addresses.first.addressLine,
              );
              final coordinatesforDropOff = Coordinates(
                double.parse(
                  tripDetails.dropLat,
                ),
                double.parse(
                  tripDetails.dropLong,
                ),
              );
              var address = await Geocoder.local
                  .findAddressesFromCoordinates(coordinatesforDropOff);
              changeDropOffAddress(address.first.addressLine);
            } else {
              await checkCurrentTripStatus('No Trip');
              // await _driverProvider.changeDriverStatus('Not Available');
              // _locationProvider.changeGoLive(false, context);
              progressDialog.hide();
              shownoDriverAlertDialog(context, response["Message"].toString());
              // FlutterRestart.restartApp();
              // Navigator.of(context).pushAndRemoveUntil(
              //     MaterialPageRoute(builder: (_) => MainScreen()),
              //     (route) => false);
            }
          }
        });
      }
    }
  }

  Future<void> shownoDriverAlertDialog(BuildContext context, message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('Trip'),
          content: Text('$message.'),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
                FlutterRestart.restartApp();
              },
            ),
          ],
        );
      },
    );
  }

  //UPDATE AVAILABILTY OF DRIVER
  Future updateCurrentStatus(String status) async {
    String driverId = _driver.id.toString();
    var data = {
      "DriverId": driverId,
      "Status": status,
    };
    await http.post(updateStatusURL, headers: webAPIKey, body: data).then(
      (value) {
        var response = json.decode(value.body);
        //STATUS
        print(response);
      },
    );
  }

  //TRIP COMPLETE FUNCTION
  Future tripComplete(BuildContext context) async {
    bool _isError = false;
    if (tripDetails != null) {
      var data = {
        "TripId": tripDetails.tripId.toString(),
        "DriverId": driver.id.toString(),
      };
      await http
          .post(
            tripCompleteURL,
            headers: webAPIKey,
            body: data,
          )
          .catchError((err) => {_isError = false})
          .then((value) async {
        var response = json.decode(value.body);
        print(response);

        if (!_isError) {
          var tripData = response["TripDetails"];
          changeTripDetails(tripData);
          checkCurrentTripStatus(tripDetails.tripStatus);
          changeDriverStatus(tripDetails.driverStatus);

          final coordinatesForPickup = Coordinates(
            double.parse(
              tripDetails.startingLat,
            ),
            double.parse(
              tripDetails.startingLong,
            ),
          );
          var addresses = await Geocoder.local
              .findAddressesFromCoordinates(coordinatesForPickup);
          changePickUpAddress(
            addresses.first.addressLine,
          );
          final coordinatesforDropOff = Coordinates(
            double.parse(
              tripDetails.dropLat,
            ),
            double.parse(
              tripDetails.dropLong,
            ),
          );
          var address = await Geocoder.local
              .findAddressesFromCoordinates(coordinatesforDropOff);
          changeDropOffAddress(address.first.addressLine);
        }
      });
    }
  }

  //INITIALISING VARIABLES
  init() {
    _isVehiclePicked = false;
    otpCode = 0;
    dropOffAddress = 'Waiting for acceptance...';
    pickUpAddress = 'Waiting for acceptance...';
    otpTextEditingController = TextEditingController();
    userNameTextEditingController = TextEditingController();
    passwordTextEditingController = TextEditingController();
    forgotPasswordController = TextEditingController();
    newPasswordTextEditingController = TextEditingController();
    confirmPasswordTextEditingController = TextEditingController();
    phoneNumberTextEditingController = TextEditingController();
    _driver = Driver();
    clear();
  }

  DriverProvider() {
    init();
  }
}
