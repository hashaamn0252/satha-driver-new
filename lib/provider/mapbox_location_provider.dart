import 'package:SathaCaptain/services/firebase_messaging.dart';
import 'package:SathaCaptain/utilis/globals.dart';
import 'package:SathaCaptain/utilis/prefs.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapBoxLocationProvider with ChangeNotifier {
  var origin;
  var destination;
  Position currentLocation;
  Location location;

  bool _goLive;
  bool get goLive => _goLive;

  PushNotificationService pushNotificationService;

  changeGoLive(bool live, BuildContext context) {
    _goLive = live;
    notifyListeners();
  }

  GoogleMapController googleMapController;

  onMapCreated(GoogleMapController controller) async {
    this.googleMapController = controller;
    LatLng position = LatLng(
        currentLocation != null ? currentLocation?.latitude : 0.0,
        currentLocation != null ? currentLocation?.longitude : 0.0);
    print('onMapCreated -> lat ->  ${currentLocation.latitude}');
    print('onMapCreated -> lng ->  ${currentLocation.longitude}');

    Future.delayed(
      Duration(milliseconds: 200),
      () async {
        this.googleMapController = controller;
        controller?.animateCamera(
          CameraUpdate?.newCameraPosition(
            CameraPosition(
              target: position,
              zoom: 15.0,
            ),
          ),
        );
      },
    );
    notifyListeners();
  }

  double lat, lng;
  changeLatLng(double latt, double lngg) {
    lat = latt;
    lng = lngg;
    notifyListeners();
  }

  onCameraMove(CameraPosition position) {
    currentLocation = Position(
      latitude: lat,
      longitude: lng,
    );
  }

  Future<Position> getUserLocation() async {
    Location location = Location();
    var ff = await location.getLocation();

    try {
      currentLocation =
          Position(longitude: ff.longitude, latitude: ff.latitude);

      return currentLocation;
    } on Exception {
      print("error");
      return currentLocation;
    }
  }

  init() async {
    _goLive = false;
    await getUserLocation();
  }

  MapBoxLocationProvider() {
    init();

    pushNotificationService = PushNotificationService();
  }
}
