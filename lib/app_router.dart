import 'package:SathaCaptain/model/userdatamodel.dart';
import 'package:SathaCaptain/screens/contact_us_screen.dart';
import 'package:SathaCaptain/screens/login_screen.dart';
import 'package:SathaCaptain/screens/main_screen.dart';
import 'package:SathaCaptain/screens/otp_screen.dart';
import 'package:SathaCaptain/screens/phone_number_screen.dart';
import 'package:SathaCaptain/screens/previous_history.dart';
import 'package:SathaCaptain/screens/recent_trip_screen.dart';
import 'package:SathaCaptain/screens/signup.dart';
import 'package:SathaCaptain/screens/splash_screen.dart';
import 'package:SathaCaptain/screens/update_password_screen.dart';
import 'package:SathaCaptain/screens/userprofile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PageViewTransition<T> extends MaterialPageRoute<T> {
  PageViewTransition({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (animation.status == AnimationStatus.reverse)
      return super
          .buildTransitions(context, animation, secondaryAnimation, child);
    return FadeTransition(opacity: animation, child: child);
  }
}

class AppRoute {
  static const String splashScreen = '/';
  static const String loginScreen = '/loginScreen';
  static const String mainScreen = '/mainScreen';
  static const String setprofiles = "/profile";
  static const String otpScreen = '/otpScreen';
  static const String phoneNumberScreen = '/phoneNumberScreen';
  static const String updatePasswordScreen = '/updatePasswordScreen';
  static const String previousHistoryScreen = '/previousHistoryScreen';
  static const String recentTripScreen = '/recentTripScreen';
  static const String contactUsScreen = '/contactUsScreen';
  static const String sigup = "/signup";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splashScreen:
        return PageViewTransition(builder: (_) => SplashScreen());
      case sigup:
        return PageViewTransition(
            builder: (_) => RegistrationScreen(
                  driverId: User.userData.mobile,
                ));

      case setprofiles:
        return PageViewTransition(
            builder: (_) => SetNewPasswordScreen(
                  mobileNumber: User.userData.mobile,
                ));
      case loginScreen:
        return PageViewTransition(builder: (_) => LoginScreen());
      case mainScreen:
        return PageViewTransition(builder: (_) => MainScreen());
      case otpScreen:
        return PageViewTransition(builder: (_) => OTPVeritifcationScreen());
      case phoneNumberScreen:
        return PageViewTransition(builder: (_) => MobileNumberScreen());
      case updatePasswordScreen:
        return PageViewTransition(builder: (_) => UpdatePasswordScreen());
      case previousHistoryScreen:
        return PageViewTransition(builder: (_) => PreviousHistory());
      case recentTripScreen:
        return PageViewTransition(builder: (_) => RecentTripScreen());
      case contactUsScreen:
        return PageViewTransition(builder: (_) => ContactUsScreen());
      default:
        return PageViewTransition(
          builder: (_) => Scaffold(
            body: Center(
              child: Text(
                'No route defined for ${settings.name}',
              ),
            ),
          ),
        );
    }
  }
}
